@extends('layouts.app2')

@section('content')
    
    <!-- Slideshow
    ============================================= -->
    <div class="owl-carousel owl-theme single-slideshow" data-autoplay="true" data-loop="true" data-autoheight="true" data-nav="true" data-items="1">
      <div class="item">
        <section class="hero-wrap">
          <div class="hero-mask opacity-7 bg-dark"></div>
          <div class="hero-bg" style="background-image:url('frontend/images/bg/image-1.jpg');"></div>
          <div class="hero-content d-flex fullscreen-with-header py-5">
            <div class="container my-auto text-center">
              <h2 class="text-16 text-white">Tilde Telecommunications LTD</h2>
              <p class="text-5 text-white mb-4">Quickly and easily buy data online with Tilde.NG<br class="d-none d-lg-block"> 
              1GB N250 </br>
              2GB N500 </br>
              3GB N75000 </br>
              4GB N1000 </br>
              5GB N1200 </br>
              10GB N2400 </br>
               </p>
              <a href="{{ route('register') }}" class="btn btn-primary m-2">Open a Free Account</a> <a class="btn btn-outline-light video-btn m-2" href="#"><span class="text-2 mr-3"><i class="fas fa-play"></i></span>See How it Works</a> </div>
          </div>
        </section>
      </div>
      <div class="item">
        <section class="hero-wrap">
          <div class="hero-bg" style="background-image:url('frontend/images/bg/image-3.jpg');"></div>
          <div class="hero-content d-flex fullscreen-with-header py-5">
            <div class="container my-auto">
              <div class="row">
                <div class="col-12 col-lg-8 col-xl-7 text-center text-lg-left">
                  <h2 class="text-13 text-white">You can chat with us on WhatsApp 07043434343</h2>
                  <p class="text-5 text-white mb-4">
              <p class="text-5 text-white mb-4">Quickly and easily buy data online with Tilde.NG<br class="d-none d-lg-block"> 
              1GB N250 </br>
              2GB N500 </br>
              3GB N750 </br>
              4GB N1000 </br>
              5GB N1200 </br>
              10GB N2400 </br></p>
                  <a href="{{ route('register') }}" class="btn btn-primary mr-3">Get started for free</a> <a class="btn btn-link text-light video-btn" href="#"><span class="mr-2"><i class="fas fa-play-circle"></i></span>Watch Demo</a> </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- Slideshow end --> 
    
    <!-- Why choose us
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <h2 class="text-9 text-center text-uppercase font-weight-400">Why choose us?</h2>
        <p class="lead text-center mb-5">Here’s Top 4 reasons why using a T&T to buy data.</p>
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <div class="row">
              <div class="col-sm-6 mb-4">
                <div class="featured-box style-3">
                  <div class="featured-box-icon border border-primary text-primary rounded-circle"> <i class="fas fa-hand-pointer"></i> </div>
                  <h3 class="font-weight-400">Easy to use</h3>
                  <p>Lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                </div>
              </div>
              <div class="col-sm-6 mb-4">
                <div class="featured-box style-3">
                  <div class="featured-box-icon border border-primary text-primary rounded-circle"> <i class="fas fa-share"></i> </div>
                  <h3 class="font-weight-400">Faster Payments</h3>
                  <p>Persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                </div>
              </div>
              <div class="col-sm-6 mb-4 mb-sm-0">
                <div class="featured-box style-3">
                  <div class="featured-box-icon border border-primary text-primary rounded-circle"> <i class="fas fa-dollar-sign"></i> </div>
                  <h3 class="font-weight-400">Lower Fees</h3>
                  <p>Essent lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="featured-box style-3">
                  <div class="featured-box-icon border border-primary text-primary rounded-circle"> <i class="fas fa-lock"></i> </div>
                  <h3 class="font-weight-400">100% secure</h3>
                  <p>Quidam lisque persius interesset his et, in quot quidam persequeris vim, ad mea essent possim iriure.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Why choose us End --> 
    
    <!-- How it works
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <h2 class="text-9 text-center text-uppercase font-weight-400">As simple as 1-2-3</h2>
        <p class="lead text-center mb-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        <div class="row">
          <div class="col-sm-4 mb-4">
            <div class="featured-box style-4">
              <div class="featured-box-icon text-dark shadow-none border-bottom"><span class="w-100 text-20 font-weight-500">1</span></div>
              <h3 class="mb-3">Sign Up Your Account</h3>
              <p class="text-3 font-weight-300">Sign up for your free Account in fea a minute.</p>
            </div>
          </div>
          <div class="col-sm-4 mb-4">
            <div class="featured-box style-4">
              <div class="featured-box-icon text-dark shadow-none border-bottom"><span class="w-100 text-20 font-weight-500">2</span></div>
              <h3 class="mb-3">Topup Your Wallet</h3>
              <p class="text-3 font-weight-300">Topup your wallet with card payment or bank transfer</p>
            </div>
          </div>
          <div class="col-sm-4 mb-4 mb-sm-0">
            <div class="featured-box style-4">
              <div class="featured-box-icon text-dark shadow-none border-bottom"><span class="w-100 text-20 font-weight-500">3</span></div>
              <h3 class="mb-3">Buy Data</h3>
              <p class="text-3 font-weight-300">The amount will be deducted from your wallet.</p>
            </div>
          </div>
        </div>
        <div class="text-center mt-2"><a href="{{ route('register') }}" class="btn btn-primary">Open a Free Account</a></div>
      </div>
    </section>
    <!-- How it works End -->

    <section class="section">
      <div class="container">
        <h2 class="text-9 text-center">Price List</h2>
        <div class="row"> 
          
          <!-- Withdrawal Funds
          ============================================= -->
          <div class="col-md-3 mb-5">
            <div class="bg-white shadow-sm rounded p-5 text-center">
              <div class="featured-box style-4">
                <div class="featured-box-icon text-light border rounded-circle shadow-none"> <i class="fas fa-hand-holding-usd"></i> </div>
                <h3 class="text-body text-7 mb-3">1GB</h3>
                <p class="text-4 line-height-4">per month</p>
                <div class="text-primary text-10 pt-3 pb-4 mb-2">₦300</div>
                </div>
            </div>
          </div>
          <!-- Withdrawal Funds end --> 
          
          <!-- Deposit Funds
          ============================================= -->
          <div class="col-md-3 mb-5">
            <div class="bg-white shadow-sm rounded p-5 text-center">
              <div class="featured-box style-4">
                <div class="featured-box-icon text-light border rounded-circle shadow-none"> <i class="fas fa-hand-holding-usd"></i> </div>
                <h3 class="text-body text-7 mb-3">2GB</h3>
                <p class="text-4 line-height-4">per month</p>
                <div class="text-primary text-10 pt-3 pb-4 mb-2">₦500</div>
                </div>
            </div>
          </div>
          <!-- Deposit Funds end --> 
          
          <!-- Receive Money
          ============================================= -->
          <div class="col-md-3 mb-5">
            <div class="bg-white shadow-sm rounded p-5 text-center">
                <div class="featured-box style-4">
                  <div class="featured-box-icon text-light border rounded-circle shadow-none"> <i class="fas fa-hand-holding-usd"></i> </div>
                    <h3 class="text-body text-7 mb-3">5GB</h3>
                    <p class="text-4 line-height-4">per month</p>
                   <div class="text-primary text-10 pt-3 pb-4 mb-2">₦1200</div>
                </div>
            </div>
          </div>
          <!-- Receive Money end --> 
          
          <!-- Send Money
          ============================================= -->
          <div class="col-md-3 mb-5">
            <div class="bg-white shadow-sm rounded p-5 text-center">
              <div class="featured-box style-4">
                <div class="featured-box-icon text-light border rounded-circle shadow-none"> <i class="fas fa-hand-holding-usd"></i> </div>
                <h3 class="text-body text-7 mb-3">10GB</h3>
                <p class="text-4 line-height-4">per month</p>
                <div class="text-primary text-10 pt-3 pb-4 mb-2">₦1,200</div>
                </div>
            </div>
          </div>
          <!-- Send Money end --> 
          
          <!-- Currency Conversion
          ============================================= -->
          <div class="col-md-3 mb-5">
            <div class="bg-white shadow-sm rounded p-5 text-center">
              <div class="featured-box style-4">
                <div class="featured-box-icon text-light border rounded-circle shadow-none"> <i class="fas fa-hand-holding-usd"></i> </div>
                <h3 class="text-body text-7 mb-3">10GB</h3>
                <p class="text-4 line-height-4">per month</p>
                <div class="text-primary text-10 pt-3 pb-4 mb-2">₦2,400</div>
                </div>
            </div>
          </div>
          <!-- Currency Conversion end --> 
          
          <!-- Administrative fee
          ============================================= -->
          <div class="col-md-3 mb-5">
            <div class="bg-white shadow-sm rounded p-5 text-center">
              <div class="featured-box style-4">
                <div class="featured-box-icon text-light border rounded-circle shadow-none"> <i class="fas fa-hand-holding-usd"></i> </div>
                <h3 class="text-body text-7 mb-3">20GB</h3>
                <p class="text-4 line-height-4">per month</p>
                <div class="text-primary text-10 pt-3 pb-4 mb-2">₦4,800</div>
                </div>
            </div>
          </div>
          <!-- Administrative fee end --> 

          <!-- Administrative fee
          ============================================= -->
          <div class="col-md-3 mb-5">
            <div class="bg-white shadow-sm rounded p-5 text-center">
              <div class="featured-box style-4">
                <div class="featured-box-icon text-light border rounded-circle shadow-none"> <i class="fas fa-hand-holding-usd"></i> </div>
                <h3 class="text-body text-7 mb-3">30GB</h3>
                <p class="text-4 line-height-4">per month</p>
                <div class="text-primary text-10 pt-3 pb-4 mb-2">₦5,000</div>
                </div>
            </div>
          </div>
          <!-- Administrative fee end --> 

          <!-- Administrative fee
          ============================================= -->
          <div class="col-md-3 mb-5">
            <div class="bg-white shadow-sm rounded p-5 text-center">
              <div class="featured-box style-4">
                <div class="featured-box-icon text-light border rounded-circle shadow-none"> <i class="fas fa-hand-holding-usd"></i> </div>
                <h3 class="text-body text-7 mb-3">40GB</h3>
                <p class="text-4 line-height-4">per month</p>
                <div class="text-primary text-10 pt-3 pb-4 mb-2">₦9,600</div>
                </div>
            </div>
          </div>
          <!-- Administrative fee end --> 
          
        </div>
      </div>
    </section>
    
    <!-- What can you do
    ============================================= -->
    <section class="section bg-white">
      <div class="container">
        <h2 class="text-9 text-center">What can you do with T&T?</h2>
        <p class="lead text-center mb-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        <div class="row">
          <div class="col-sm-6 col-lg-4 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"> <i class="fas fa-share-square"></i> </div>
              <h3>Topup Account</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-4 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"> <i class="fas fa-check-square"></i> </div>
              <h3>Buy Data</h3>
            </div>
            </a> </div>
          <div class="col-sm-6 col-lg-4 mb-4"> <a href="#">
            <div class="featured-box style-5 rounded">
              <div class="featured-box-icon text-primary"> <i class="fas fa-user-friends"></i> </div>
              <h3>Pay for a Friend</h3>
            </div>
            </a> </div>
        </div>
        <div class="text-center mt-4"><a href="#" class="btn-link text-4">See more can you do<i class="fas fa-chevron-right text-2 ml-2"></i></a></div>
      </div>
    </section>
    <!-- What can you do end --> 
    
    <!-- Customer Support
    ============================================= -->
    <section class="hero-wrap section shadow-md">
      <div class="hero-mask opacity-9 bg-primary"></div>
      <div class="hero-bg" style="background-image:url('frontend/images/bg/image-2.jpg');"></div>
      <div class="hero-content py-5">
        <div class="container text-center">
          <h2 class="text-9 text-white">Awesome Customer Support</h2>
          <p class="lead text-white mb-4">Have you any query? Don't worry. We have great people ready to help you whenever you need it.</p>
          <a href="#" class="btn btn-light">Find out more</a> </div>
      </div>
    </section>
    <!-- Customer Support end --> 
    
    <!-- Mobile App
    ============================================= -->
    <section class="section py-5">
      <div class="container">
        <div class="justify-content-center text-center">
          <h2 class="text-9">Get the app</h2>
          <p class="lead mb-4">Download our app for the fastest, most convenient way to send & get Payment.</p>
          <a class="d-inline-flex mx-3" href=""><img alt="" width="168" height="49"  src="{{ asset('frontend/images/app-store.png') }}"></a> <a class="d-inline-flex mx-3" href=""><img alt="" width="166" height="49"  src="{{ asset('frontend/images/google-play-store.png') }}"></a> </div>
      </div>
    </section>
    <!-- Mobile App end --> 

@endsection