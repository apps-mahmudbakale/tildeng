@extends('layouts.app2')

@section('content')
    <div class="container">
        <div class="row"> 
        <!-- Left Panel
        ============================================= -->
        <aside class="col-lg-3"> 
          
          <!-- Profile Details
          =============================== -->
          <div class="bg-white shadow-sm rounded text-center p-3 mb-4">
            <div class="profile-thumb mt-3 mb-4"> <img class="rounded-circle" src="{{ Auth::user()->image == null ? asset('frontend/images/profile-thumb.jpg') : Auth::user()->image }}" alt="" style="max-width: 100px; max-height: 100px; display: block;">
              <div class="profile-thumb-edit custom-file bg-primary text-white" data-toggle="tooltip" title="Change Profile Picture"> <i class="fas fa-camera position-absolute"></i>
                <input type="file" class="custom-file-input" id="customFile" accept="image/*"/>
              </div>
            </div>
            <p class="text-3 font-weight-500 mb-2">Hello, {{ Auth::user()->name }} ({{ Auth::user()->phone }})</p>
            <p class="text-2">{{ Auth::user()->status == 1 ? 'Customer' : 'Agent' }}</p>
            <p class="mb-2"><a href="{{ url('profile') }}" class="text-5 text-light" data-toggle="tooltip" title="Edit Profile"><i class="fas fa-edit"></i></a></p>
          </div>
          <!-- Profile Details End --> 
          
          <!-- Available Balance
          =============================== -->
          <div class="bg-white shadow-sm rounded text-center p-3 mb-4">
            <div class="text-17 text-light my-3"><i class="fas fa-wallet"></i></div>
            <h3 class="text-9 font-weight-400">₦{{ number_format(Auth::user()->wallet->balance, 2) }}</h3>
            <p class="mb-2 text-muted opacity-8">Available Balance</p>
            <hr class="mx-n3">
            <div class="d-flex"><a href="{{ route('topup') }}" class="btn-link mr-auto">Topup</a> <a href="{{ route('buy') }}" class="btn-link ml-auto">Buy Data</a></div>
          </div>
          <!-- Available Balance End --> 
          
          <!-- Need Help?
          =============================== -->
          <div class="bg-white shadow-sm rounded text-center p-3 mb-4">
            <div class="text-17 text-light my-3"><i class="fas fa-comments"></i></div>
            <h3 class="text-5 font-weight-400 my-4">Need Help?</h3>
            <p class="text-muted opacity-8 mb-4">Have questions or concerns regrading your account?<br>
              Our experts are here to help!.</p>
            <a href="https://wa.me/message/NHVUBD2UHFIZJ1" target="_blank" class="btn btn-primary btn-block">Chat with Us</a> </div>
          <!-- Need Help? End --> 
          
        </aside>
        <!-- Left Panel End --> 
        
        <!-- Middle Panel
        ============================================= -->
        <div class="col-lg-9"> 
          <!-- Recent Activity
          =============================== -->
          <div class="bg-white shadow-sm rounded py-4 mb-4">
            <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">Recent Transactions</h3>
            
            <!-- Title
            =============================== -->
            <div class="transaction-title py-2 px-4">
              <div class="row font-weight-00">
                <div class="col-2 col-sm-1 text-center"><span class="">Date</span></div>
                <div class="col col-sm-3">Description</div>
                <div class="col col-sm-4">Phone</div>
                <div class="col col-sm-2 text-center">Status</div>
                <div class="col-3 col-sm-2 text-right">Amount</div>
              </div>
            </div>
            <!-- Title End --> 
            
            <!-- Transaction List
            =============================== -->
            <div class="transaction-list">
                @foreach($recentPurchases as $key => $transaction)
                  <div class="transaction-item px-4 py-3" data-toggle="modal" data-target="#transaction-detail" data-description="Data Purchase" data-network="{{ $transaction->product->category->name }}" data-product="{{ $transaction->product->name }}" data-amount="₦{{ $transaction->product->price }}" data-reference="{{ $transaction->reference_no }}" data-status="{{ $transaction->status == 2 ? 'Approved' : 'In Progress' }}" data-date="{{ Carbon\Carbon::parse($transaction->created_at)->format('Y-m-d H:i') }}" data-paidby="wallet">
                <div class="row align-items-center flex-row">
                  <div class="col-2 col-sm-1 text-center"> <span class="d-block text-4 font-weight-300">{{ Carbon\Carbon::parse($transaction->created_at)->format('d H:i') }}</span> <span class="d-block text-1 font-weight-300 text-uppercase">{{ Carbon\Carbon::parse($transaction->created_at)->format('M') }}</span> </div>
                  <div class="col col-sm-3"> <span class="d-block text-4">{{ $transaction->product->name }}</span> <span class="text-muted">Data Purchase</span> </div>
                  <div class="col col-sm-4"> <span class="d-block text-4">{{ $transaction->phone }}</span></div>
                  
                  
                   @if($transaction->status == 2)
                    <div class="col-sm-2 text-center text-3"> <span class="text-success" data-toggle="tooltip" data-original-title="Approved"><i class="fas fa-check-circle"></i> Approved</span> </div>
            
                  @else
                    <div class="col-sm-2 text-center text-3"> <span class="text-danger" data-toggle="tooltip" data-original-title="In Progress"><i class="fas fa-times-circle"></i> In Progress</span> </div>
                    
                
                  @endif
                  <div class="col-3 col-sm-2 text-right text-4"> <span class="text-nowrap">- ₦{{ $transaction->product->price }}</span> <span class="text-2 text-uppercase">(NGN)</span> </div>
                </div>
              </div>
                @endforeach
            </div>
            <!-- Transaction List End --> 
            
            <!-- Transaction Item Details Modal
            =========================================== -->
            <div id="transaction-detail" class="modal fade" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered transaction-details" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <div class="row no-gutters">
                      <div class="col-sm-5 d-flex justify-content-center bg-primary rounded-left py-4">
                        <div class="my-auto text-center">
                          <div class="text-17 text-white my-3"><i class="fas fa-building"></i></div>
                          <h3 class="text-4 text-white font-weight-400 my-3">Tilde Telecommunication Ltd</h3>
                          <div class="text-8 font-weight-500 text-white my-4" id="amount"></div>
                          <p class="text-white" id="date"></p>
                        </div>
                      </div>
                      <div class="col-sm-7">
                        <h5 class="text-5 font-weight-400 m-3">Transaction Details
                          <button type="button" class="close font-weight-400" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        </h5>
                        <hr>
                        <div class="px-3">
                          <ul class="list-unstyled">
                            <li class="mb-2">Payment Amount <span class="float-right text-3" id="amount">$562.00</span></li>
                            <li class="mb-2">Fee <span class="float-right text-3">-₦0.00</span></li>
                          </ul>
                          <hr class="mb-2">
                          <p class="d-flex align-items-center font-weight-500 mb-4">Total Amount <span class="text-3 ml-auto" id="amount"></span></p>
                          <ul class="list-unstyled">
                            <li class="font-weight-500">Paid By:</li>
                            <li class="text-muted" id="phone"></li>
                          </ul>
                          <ul class="list-unstyled">
                            <li class="font-weight-500">Transaction ID:</li>
                            <li class="text-muted" id="reference"></li>
                          </ul>
                          <ul class="list-unstyled">
                            <li class="font-weight-500">Description:</li>
                            <li class="text-muted" id="product"></li>
                          </ul>
                          <ul class="list-unstyled">
                            <li class="font-weight-500">Status:</li>
                            <li class="text-muted" id="status"><span class="text-success text-3 ml-1"><i class="fas fa-check-circle"></i></span></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Transaction Item Details Modal End --> 
            
            <!-- View all Link
            =============================== -->
            <div class="text-center mt-4"><a href="{{ url('transactions') }}" class="btn-link text-3">View all<i class="fas fa-chevron-right text-2 ml-2"></i></a></div>
            <!-- View all Link End --> 
            
          </div>
          <!-- Recent Activity End -->

          <!-- Recent Activity
          =============================== -->
          <div class="bg-white shadow-sm rounded py-4 mb-4">
            <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">Recent Topups</h3>
            
            <!-- Title
            =============================== -->
            <div class="transaction-title py-2 px-4">
              <div class="row font-weight-00">
                <div class="col-2 col-sm-1 text-center"><span class="">Date</span></div>
                <div class="col col-sm-3">Description</div>
                <div class="col col-sm-4">Phone</div>
                <div class="col-auto col-sm-2 d-none d-sm-block text-center">Status</div>
                <div class="col-3 col-sm-2 text-right">Amount</div>
              </div>
            </div>
            <!-- Title End --> 
            
            <!-- Transaction List
            =============================== -->
            <div class="transaction-list">
                @foreach($recentTopup as $key => $transaction)
                  <div class="transaction-item px-4 py-3" data-toggle="modal" data-target="#transaction-detail" data-description="Wallet Topup" data-network="Topup" data-product="{{ auth()->user()->phone }}" data-amount="₦{{ $transaction->amount }}" data-reference="{{ $transaction->uuid }}" data-status="{{ $transaction->confirmed == 1 ? 'Approved' : 'In Progress' }}" data-date="{{ Carbon\Carbon::parse($transaction->created_at)->format('d-M-Y') }}" data-paidby="Paystack">
                <div class="row align-items-center flex-row">
                  <div class="col-2 col-sm-1 text-center"> <span class="d-block text-4 font-weight-300">{{ Carbon\Carbon::parse($transaction->created_at)->format('d') }}</span> <span class="d-block text-1 font-weight-300 text-uppercase">{{ Carbon\Carbon::parse($transaction->created_at)->format('M') }}</span> </div>
                  <div class="col col-sm-3"> <span class="d-block text-4">{{ $transaction->amount }}</span> <span class="text-muted">Wallet Topup</span> </div>
                  <div class="col col-sm-4"> <span class="d-block text-4">{{ auth()->user()->phone }}</span></div>
                  @if($transaction->confirmed  == 1)
                    <div class="col-auto col-sm-2 d-none d-sm-block text-center text-3"> <span class="text-success" data-toggle="tooltip" data-original-title="Approved"><i class="fas fa-check-circle"></i> Approved</span> </div>
                  @else
                    <div class="col-auto col-sm-2 d-none d-sm-block text-center text-3"> <span class="text-danger" data-toggle="tooltip" data-original-title="In Progress"><i class="fas fa-times-circle"></i> In Progress</span> </div>
                    
                  @endif
                  <div class="col-3 col-sm-2 text-right text-4"> <span class="text-nowrap">+ ₦{{ $transaction->amount }}</span> <span class="text-2 text-uppercase">(NGN)</span> </div>
                </div>
              </div>
                @endforeach
            </div>
            <!-- Transaction List End --> 
            
            <!-- View all Link
            =============================== -->
            <div class="text-center mt-4"><a href="{{ url('topups') }}" class="btn-link text-3">View all<i class="fas fa-chevron-right text-2 ml-2"></i></a></div>
            <!-- View all Link End --> 
            
          </div>
          <!-- Recent Activity End --> 
        </div>
        <!-- Middle Panel End --> 
      </div>
    </div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
  $(document).ready(function() {
   $(".transaction-item").click(function () {
            var amount = $(this).data('amount');
            var reference = $(this).data('reference');
            var product = $(this).data('product');
            var network = $(this).data('network');
            var date = $(this).data('date');
            var status = $(this).data('status');
            var phone = $(this).data('phone');

            $(".modal-body #amount").text(amount);
            $(".modal-body #reference").text(reference);
            $(".modal-body #product").text(network + '/' + product);
            $(".modal-body #date").text(date);
            $(".modal-body #status").text(status);
            $(".modal-body #phone").text(phone);
        })
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.rounded-circle').attr('src', e.target.result);
                var formData = new FormData();
                formData.append('fileData', e.target.result);
                 $.ajax({
                  url: '/profile-picture',
                  method: 'post',
                  data: formData,
                  contentType : false,
                  processData : false,
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  success: function(response){
                      console.log(response);
                    }

                });
            }
            reader.readAsDataURL(input.files[0]);


        }
    }
    

    $(".custom-file-input").on('change', function(){
        readURL(this);
    });
    
    $(".position-absolute").on('click', function() {
       $(".custom-file-input").click();
    });
});
</script>