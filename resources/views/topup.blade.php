@extends('layouts.app2')

@section('secondary-menu')
    <!-- Secondary menu
          ============================================= -->
    <div class="bg-white">
        <div class="container d-flex justify-content-center">
            <ul class="nav secondary-nav alternate">
                <li class="nav-item"> <a class="nav-link active" href="{{ route('topup') }}">Topup</a></li>
                <li class="nav-item"> <a class="nav-link" href="{{ route('buy') }}">Buy Data</a></li>
            </ul>
        </div>
    </div>
    <!-- Secondary menu end -->
@stop

@section('content')
    <div class="container">

        <!-- Steps Progress bar -->
        <div class="row mt-4 mb-5">
            <div class="col-lg-11 mx-auto">
                <div class="row widget-steps">
                    <div class="col-4 step active">
                        <div class="step-name">Details</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="#" class="step-dot"></a>
                    </div>
                    <div class="col-4 step disabled">
                        <div class="step-name">Confirm</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="#" class="step-dot"></a>
                    </div>
                    <div class="col-4 step disabled">
                        <div class="step-name">Success</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="#" class="step-dot"></a>
                    </div>
                </div>
            </div>
        </div>
        <h2 class="font-weight-400 text-center mt-3 mb-4">Wallet Topup</h2>
        <div class="row">
            <div class="col-md-9 col-lg-7 col-xl-6 mx-auto">
                <div class="bg-white shadow-sm rounded p-3 pt-sm-5 pb-sm-5 px-sm-5 mb-4">

                    <!-- Deposit Money Form
                    ============================================= -->
                    {{-- <form id="form-send-money" method="post" action="{{ route('pay') }}"> --}}
                    <div class="form-group">
                        <label for="youSend">Amount</label>
                        <div class="input-group">
                            <div class="input-group-prepend"> <span class="input-group-text">₦</span> </div>
                            <input type="text" name="amt" class="form-control" data-bv-field="youSend" id="amount"
                                value="" placeholder="1000" onkeyup="showAmount();">
                            <div class="input-group-append"> <span class="input-group-text p-0">
                                    <select id="youSendCurrency" data-style="custom-select bg-transparent border-0"
                                        data-container="body" data-live-search="true"
                                        class="selectpicker form-control bg-transparent" required="">
                                        <optgroup label="Popular Currency">
                                            <option data-icon="currency-flag currency-flag-ngn mr-1"
                                                data-subtext="Nigerian naira" selected="selected" value="">NGN
                                            </option>
                                            <option data-icon="currency-flag currency-flag-usd mr-1"
                                                data-subtext="United States dollar" value="">USD</option>
                                        </optgroup>
                                    </select>
                                </span> </div>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                <label for="paymentMethod">Payment Method</label>
                <select id="cardType" class="custom-select" required="" name="payment_type">
                  <option value="">Select Payment Method</option>
                  <option value="online">Online Payment</option>
                  <option value="transfer">Bank/Transfer</option>
                </select>
              </div> --}}
                    <p class="text-muted mt-4">Transactions fees <span
                            class="float-right d-flex align-items-center"><del>1.00 NGN</del> <span
                                class="bg-info text-1 text-white font-weight-500 rounded d-inline-block px-2 line-height-4 ml-2">Free</span></span>
                    </p>
                    <hr>
                    <p class="text-4 font-weight-500">You'll deposit <span class="float-right" id="showAmt"></span>
                    </p>
                    <button type="submit" id="btnPay" class="btn btn-primary btn-block" onclick="payWithPaystack()"> Pay </button>
                    </form>
                    <!-- Deposit Money Form end -->
                </div>
            </div>
        </div>
    </div>
    <script src="https://js.paystack.co/v1/inline.js"></script>
    <script>
        const btnPay = document.getElementById('btnPay');

        btnPay.addEventListener("click", payWithPaystack, false);

        function payWithPaystack(e) {

            e.preventDefault();


            let handler = PaystackPop.setup({

                key: 'pk_live_3b8acd04daf939a25f3e9d8a9a8bc956ea0cf1e7', // Replace with your public key

                email: '{{ auth()->user()->email }}',

                amount: document.getElementById("amount").value * 100,

                ref: '' + Math.floor((Math.random() * 1000000000) +
                1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you

                // label: "Optional string that replaces customer email"

                onClose: function() {

                    alert('Transaction was not completed, window closed.');

                },

                callback: function(response) {

                    let Url = 'https://tilde.ng/';
                    let amount = document.getElementById('amount');

                    window.location = Url + 'FundWallet/' + amount.value;

                }

            });


            handler.openIframe();

        }
    </script>
    <script>
        function showAmount() {
            // Selecting the input element and get its value 
            var amount = document.getElementById("amount").value;

            // Displaying the value
            document.getElementById('showAmt').innerHTML = amount + ' NGN';
            document.getElementById('theAmount').value = amount + '00';
        }
    </script>

@endsection
