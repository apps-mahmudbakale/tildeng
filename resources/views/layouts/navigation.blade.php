<nav class="side-nav">
    <a href="{{ route('dashboard') }}" class="intro-x flex items-center pl-5 pt-4">
        
        <x-application-logo class="w-6" />
        <span class="hidden xl:block text-white text-lg ml-3"> T<span class="font-medium">$</span>T </span>
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul>
        <li>
            @if(Auth::guard('admin')->check())
                <x-nav-link href="{{ route('admin.dashboard') }}" :active="request()->routeIs('admin.dashboard')">
                    <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                    <div class="side-menu__title"> Dashboard </div>
                </x-nav-link>

                <x-nav-link href="{{ route('users.index') }}" :active="request()->routeIs('users.index')">
                    <div class="side-menu__icon"> <i data-feather="users"></i> </div>
                    <div class="side-menu__title"> Users </div>
                </x-nav-link>
                 
                <x-nav-link href="{{ route('agents.index') }}" :active="request()->routeIs('agents.index')">
                    <div class="side-menu__icon"> <i data-feather="users"></i> </div>
                    <div class="side-menu__title"> Agents </div>
                </x-nav-link>

                <li>
                    <a href="javascript:;" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="credit-card"></i> </div>
                        <div class="side-menu__title">
                            Purchases 
                            <div class="side-menu__sub-icon "> <i data-feather="chevron-down"></i> </div>
                        </div>
                    </a>
                    <ul class="">
                        <li>
                            <a href="{{ route('purchases.index', 'type=New') }}" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="side-menu__title"> New Purchase </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('purchases.index', 'type=Approved') }}" class="side-menu">
                                <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                <div class="side-menu__title"> Approved Purchase </div>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="side-nav__devider my-6"></li>
                    <li>
                        <a href="javascript:;" class="side-menu">
                            <div class="side-menu__icon"> <i data-feather="inbox"></i> </div>
                            <div class="side-menu__title">
                                Settings 
                                <div class="side-menu__sub-icon "> <i data-feather="chevron-down"></i> </div>
                            </div>
                        </a>
                        <ul class="">
                            <li>
                                <a href="{{ route('categories.index') }}" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Networks </div>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('products.index') }}" class="side-menu">
                                    <div class="side-menu__icon"> <i data-feather="activity"></i> </div>
                                    <div class="side-menu__title"> Products </div>
                                </a>
                            </li>
                        </ul>
                    </li>
            @endif
        </li>
    </ul>
</nav>
