<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
<link href="images/favicon.png" rel="icon" />
<title>{{ config('app.name') }}</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="description" content="This professional design html template is for build a Money Transfer and online payments website.">
<meta name="author" content="harnishdesign.net">

<!-- Web Fonts
============================================= -->
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i' type='text/css'>

<!-- Stylesheet
============================================= -->
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/bootstrap/css/bootstrap.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/font-awesome/css/all.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/owl.carousel/assets/owl.carousel.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/bootstrap-select/css/bootstrap-select.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/currency-flags/css/currency-flags.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/stylesheet.css') }}" />
</head>
<body>

<!-- Preloader -->
<div id="preloader">
  <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End --> 

<!-- Document Wrapper   
============================================= -->
<div id="main-wrapper"> 
  
  <!-- Header
  ============================================= -->
  <header id="header">
    <div class="container">
      <div class="header-row">
        <div class="header-column justify-content-start"> 
          <!-- Logo
          ============================= -->
          {{-- <div class="logo"> <a class="d-flex" href="{{ url('/') }}" title="Tilde Telecommunications"><img src="{{ asset('frontend/images/logo.png') }}" width="121" height="33"  alt="Payyed" /></a> </div> --}}
          <!-- Logo end --> 
          <!-- Collapse Button
          ============================== -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav"> <span></span> <span></span> <span></span> </button>
          <!-- Collapse Button end --> 
          
          <!-- Primary Navigation
          ============================== -->
          <nav class="primary-menu navbar navbar-expand-lg">
            <div id="header-nav" class="collapse navbar-collapse">
              <ul class="navbar-nav mr-auto">
                <li><a href="{{ url('/') }}">Home</a></li>
                @if(Auth::check())
                <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                @endif
                <li><a href="{{ route('topup') }}">Top Up</a></li>
                <li><a href="{{ route('buy') }}">Buy Data</a></li>
                <li><a href="#">Contact Us</a></li>
              </ul>
            </div>
          </nav>
          <!-- Primary Navigation end --> 
        </div>
        <div class="header-column justify-content-end">
          @if(Auth::check())
            <!-- My Profile
          ============================== -->
          <nav class="login-signup navbar navbar-expand">
            <ul class="navbar-nav">
              <li class="dropdown language"> <a class="dropdown-toggle" href="#">En</a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">English</a></li>
                </ul>
              </li>
              <li class="dropdown notifications"> <a class="dropdown-toggle" href="#"><span class="text-5"><i class="far fa-bell"></i></span><span class="count">3</span></a>
                <ul class="dropdown-menu">
                  <li class="text-center text-3 py-2">Notifications (3)</li>
                  <li class="dropdown-divider mx-n3"></li>
                  <li><a class="dropdown-item" href="#"><i class="fas fa-bell"></i>A new digital FIRC document is available for you to download<span class="text-1 text-muted d-block">22 Jul 2020</span></a></li>
                  <li><a class="dropdown-item" href="#"><i class="fas fa-bell"></i>Updates to our privacy policy. Please read.<span class="text-1 text-muted d-block">04 March 2020</span></a></li>
                  <li><a class="dropdown-item" href="#"><i class="fas fa-bell"></i>Update about Payyed fees<span class="text-1 text-muted d-block">18 Feb 2020</span></a></li>
                  <li class="dropdown-divider mx-n3"></li>
                  <li><a class="dropdown-item text-center text-primary px-0" href="notifications.html">See all Notifications</a></li>
                </ul>
              </li>
              <li class="dropdown profile ml-2"> <a class="px-0 dropdown-toggle" href="#"><img class="rounded-circle" src="{{ Auth::user()->image == null ? asset('frontend/images/profile-thumb.jpg') : Auth::user()->image }}" alt="" style="max-width: 50px; max-height: 50px; display: block;"></a>
                <ul class="dropdown-menu">
                  <li class="text-center text-3 py-2">Hi, {{ Auth::user()->name  }}</li>
                  <li class="dropdown-divider mx-n3"></li>
                  <li><a class="dropdown-item" href="{{ url('/profile') }}"><i class="fas fa-user"></i>My Profile</a></li>
                  <li><a class="dropdown-item" href="{{ url('/security') }}"><i class="fas fa-shield-alt"></i>Security</a></li>
                  <li><a class="dropdown-item" href="{{ url('/payment-methods') }}"><i class="fas fa-credit-card"></i>Payment Methods</a></li>
                  <li><a class="dropdown-item" href="{{ url('/notifications') }}"><i class="fas fa-bell"></i>Notifications</a></li>
                  <li class="dropdown-divider mx-n3"></li>
                  <li><a class="dropdown-item" href="{{ url('/help') }}"><i class="fas fa-life-ring"></i>Need Help?</a></li>
                  
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        this.closest('form').submit();" class="dropdown-item"><i class="fas fa-sign-out-alt"></i>Sign Out</a>
                                        </li>
                    </form>
                  
                </ul>
              </li>
            </ul>
          </nav>
          <!-- My Profile end --> 
          @else
            <!-- Login & Signup Link
          ============================== -->
          <nav class="login-signup navbar navbar-expand">
            <ul class="navbar-nav">
              <li><a href="{{ route('login') }}">Login</a> </li>
              <li class="align-items-center h-auto ml-sm-3"><a class="btn btn-primary" href="{{ route('register') }}">Sign Up</a></li>
            </ul>
          </nav>
          <!-- Login & Signup Link end --> 
          @endif
        </div>
      </div>
    </div>
  </header>
  <!-- Header End --> 
  
  @yield('secondary-menu')

  <!-- Content
  ============================================= -->
  <div id="content" @if(!Request::is('/')) class="py-4" @endif> 
    
    @yield('content')
    
  </div>
  <!-- Content end --> 
  
  <!-- Footer
  ============================================= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg d-lg-flex align-items-center">
          <ul class="nav justify-content-center justify-content-lg-start text-3">
            <li class="nav-item"> <a class="nav-link active" href="#">About Us</a></li>
            <li class="nav-item"> <a class="nav-link" href="#">Servicees</a></li>
            <li class="nav-item"> <a class="nav-link" href="#">Help</a></li>
            <li class="nav-item"> <a class="nav-link" href="#">Services</a></li>
            <li class="nav-item"> <a class="nav-link" href="#">Price</a></li>
            <li class="nav-item"> <a class="nav-link" href="#">Contact Us</a></li>
          </ul>
        </div>
        <div class="col-lg d-lg-flex justify-content-lg-end mt-3 mt-lg-0">
          <ul class="social-icons justify-content-center">
            <li class="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
            <li class="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
            <li class="social-icons-google"><a data-toggle="tooltip" href="http://www.google.com/" target="_blank" title="Google"><i class="fab fa-google"></i></a></li>
            <li class="social-icons-youtube"><a data-toggle="tooltip" href="http://www.youtube.com/" target="_blank" title="Youtube"><i class="fab fa-youtube"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="footer-copyright pt-3 pt-lg-2 mt-2">
        <div class="row">
          <div class="col-lg">
            <p class="text-center text-lg-left mb-2 mb-lg-0">Copyright &copy; {{ date('Y') }} <a href="#">Tilde Telecommunication Ltd</a>. All Rights Reserved.</p>
          </div>
          <div class="col-lg d-lg-flex align-items-center justify-content-lg-end">
            <ul class="nav justify-content-center">
              <li class="nav-item"> <a class="nav-link active" href="#">Security</a></li>
              <li class="nav-item"> <a class="nav-link" href="#">Terms</a></li>
              <li class="nav-item"> <a class="nav-link" href="#">Privacy</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer end --> 
  
</div>
<!-- Document Wrapper end --> 

<!-- Back to Top
============================================= --> 
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)"><i class="fa fa-chevron-up"></i></a> 

<!-- Video Modal
============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-transparent border-0">
      <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="modal-body p-0">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Video Modal end --> 

<!-- Script --> 
<script src="{{ asset('frontend/vendor/jquery/jquery.min.js') }}"></script> 
<script src="{{ asset('frontend/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('frontend/vendor/bootstrap-select/js/bootstrap-select.min.js') }}"></script> 
<script src="{{ asset('frontend/vendor/owl.carousel/owl.carousel.min.js') }}"></script> 
<script src="{{ asset('frontend/js/theme.js') }}"></script>
</body>
</html>