<button {{ $attributes->merge(['type' => 'submit', 'class' => 'button button--lg w-full']) }}>
    {{ $slot }}
</button>
