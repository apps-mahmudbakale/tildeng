@props(['active'])

@php
$classes = ($active ?? false)
            ? 'side-menu side-menu--active'
            : 'side-menu';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>

