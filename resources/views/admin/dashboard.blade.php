<x-app-layout>
    <div class="col-span-12 xxl:col-span-9 grid grid-cols-12 gap-6">
                        <!-- BEGIN: General Report -->
                        <div class="col-span-12 mt-8">
                                <div class="intro-y flex items-center h-10">
                                    <h2 class="text-lg font-medium truncate mr-5">
                                        Dashboard
                                    </h2>
                                    <a href="" class="ml-auto flex items-center text-theme-1 dark:text-theme-10"> <i data-feather="refresh-ccw" class="w-4 h-4 mr-3"></i> Reload Data </a>
                                </div>
                                <div class="grid grid-cols-12 gap-6 mt-5">
                                    <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                        <div class="report-box zoom-in">
                                            <div class="box p-5">
                                                <div class="flex">
                                                    <i data-feather="credit-card" class="report-box__icon text-theme-10"></i> 
                                                    <div class="ml-auto">
                                                        <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer" title="33% Higher than last month"> 33% <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i> </div>
                                                    </div>
                                                </div>
                                                <div class="text-3xl font-medium leading-8 mt-6">{{ number_format($totalProducts) }}</div>
                                                <div class="text-base text-gray-600 mt-1">Products</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                        <div class="report-box zoom-in">
                                            <div class="box p-5">
                                                <div class="flex">
                                                    <i data-feather="shopping-cart" class="report-box__icon text-theme-11"></i> 
                                                    <div class="ml-auto">
                                                        <div class="report-box__indicator bg-theme-6 tooltip cursor-pointer" title="2% Lower than last month"> 2% <i data-feather="chevron-down" class="w-4 h-4 ml-0.5"></i> </div>
                                                    </div>
                                                </div>
                                                <div class="text-3xl font-medium leading-8 mt-6">{{ number_format($totalTopups, 2) }}</div>
                                                <div class="text-base text-gray-600 mt-1">Total Topups</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                        <div class="report-box zoom-in">
                                            <div class="box p-5">
                                                <div class="flex">
                                                    <i data-feather="users" class="report-box__icon text-theme-12"></i> 
                                                    <div class="ml-auto">
                                                        <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer" title="12% Higher than last month"> 12% <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i> </div>
                                                    </div>
                                                </div>
                                                <div class="text-3xl font-medium leading-8 mt-6">{{ number_format($totalCustomers, 2) }}</div>
                                                <div class="text-base text-gray-600 mt-1">Customers</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                                        <div class="report-box zoom-in">
                                            <div class="box p-5">
                                                <div class="flex">
                                                    <i data-feather="users" class="report-box__icon text-theme-9"></i> 
                                                    <div class="ml-auto">
                                                        <div class="report-box__indicator bg-theme-9 tooltip cursor-pointer" title="22% Higher than last month"> 22% <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i> </div>
                                                    </div>
                                                </div>
                                                <div class="text-3xl font-medium leading-8 mt-6">{{ number_format($totalAgents, 2) }}</div>
                                                <div class="text-base text-gray-600 mt-1">Agents</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- END: General Report -->
                       
                        
                        
                       
                        <!-- BEGIN: Weekly Top Products -->
                        <div class="col-span-12 mt-6">
                                <div class="intro-y block sm:flex items-center h-10">
                                    <h2 class="text-lg font-medium truncate mr-5">
                                        Recent Purchases
                                    </h2>
                                    <div class="flex items-center sm:ml-auto mt-3 sm:mt-0">
                                        <button class="btn box flex items-center text-gray-700 dark:text-gray-300"> <i data-feather="file-text" class="hidden sm:block w-4 h-4 mr-2"></i> Export to Excel </button>
                                        <button class="ml-3 btn box flex items-center text-gray-700 dark:text-gray-300"> <i data-feather="file-text" class="hidden sm:block w-4 h-4 mr-2"></i> Export to PDF </button>
                                    </div>
                                </div>
                                <div class="intro-y overflow-auto lg:overflow-visible mt-8 sm:mt-0">
                                    <table class="table table-report sm:mt-2">
                                        <thead>
                                            <tr>
                                                <th class="whitespace-nowrap">CUSTOMER</th>
                                                <th class="text-center whitespace-nowrap">PRODUCTs</th>
                                                <th class="text-center whitespace-nowrap">PHONE</th>
                                                <th class="text-center whitespace-nowrap">PRICE</th>
                                                <th class="text-center whitespace-nowrap">DATE</th>
                                                <th class="text-center whitespace-nowrap">STATUS</th>
                                                <th class="text-center whitespace-nowrap">ACTIONS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($recentPurchases as $purchase)
                                            <tr class="intro-x">
                                                <td>
                                                    <a href="" class="font-medium whitespace-nowrap">{{ @$purchase->user->name }}</a> 
                                                    <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">{{ @$purchase->user->status == 1 ? 'Customer' : 'Agent' }}</div>
                                                </td>
                                                <td>
                                                    <a href="" class="font-medium whitespace-nowrap">{{ $purchase->product->category->name }}</a> 
                                                    <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">{{ $purchase->product->name }}</div>
                                                </td>
                                                <td class="text-center">{{ $purchase->phone }}</td>
                                                <td class="text-center">{{ number_format($purchase->product->price) }}</td>
                                                <td class="text-center">{{ Carbon\Carbon::parse($purchase->created_at)->format('d-m H:i') }}</td>
                                                @if($purchase->status == 2)
                                                <td class="w-40">
                                                    
                                                        <div class="flex items-center justify-center text-theme-9"> <i data-feather="check-square" class="w-4 h-4 mr-2"></i> Approved </div>
                                                </td>
                                                <td class="table-report__action w-56">
                                                    <div class="flex items-center justify-center text-theme-9"> <i data-feather="check-square" class="w-4 h-4 mr-2"></i> {{ $purchase->admin->name }}  </div>
                                                </td>
                                                    @else
                                                    <td class="w-40">
                                                        <div class="flex items-center justify-center text-theme-6"> <i data-feather="check-square" class="w-4 h-4 mr-2"></i> In Progress </div>
                                                    </td>

                                                    <td class="table-report__action w-56">
                                                    <div class="flex justify-center items-center">
                                                        
                                                        <a href="{{ url('admin/purchases/approve/' .$purchase->id) }}" class="flex mr-3 btn btn-sm btn-success" onclick="return confirm('Are you sure you want to approve?')">
                                            <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Approve
                                        </a>
                                                    </div>
                                                </td>
                                                    @endif
                                                
                                                
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <!-- END: Weekly Top Products -->
                    </div>
                    
</x-app-layout>
