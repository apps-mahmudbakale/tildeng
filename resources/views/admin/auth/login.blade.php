<x-guest-layout>

    <x-auth-card>
        <x-slot name="header">
            Login
        </x-slot>

        <x-slot name="info">
            Admin Login
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('admin.login') }}">
            @csrf

            <div class="intro-x mt-8">
                <div class="input-form @error('email') has-error @enderror">
                    <x-input id="email" class="intro-x login__input form-control py-3 px-4 border-gray-300 block" type="email" name="email" value="{{ old('email') }}" autofocus placeholder="Email" />
                    @error('email')
                        <div class="pristine-error text-theme-6 mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="input-form @error('password') has-error @enderror">
                    <x-input id="password" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4"
                                type="password"
                                name="password"
                                placeholder="Password"
                                autocomplete="current-password" />
                    @error('password')
                        <div class="pristine-error text-theme-6 mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
            </div>
            <div class="intro-x flex text-gray-700 dark:text-gray-600 text-xs sm:text-sm mt-4">
                <div class="flex items-center mr-auto">
                    <input id="remember-me" type="checkbox" name="remember" class="form-check-input border mr-2">
                    <label class="cursor-pointer select-none" for="remember-me">{{ __('Remember me') }}</label>
                </div>
                @if (Route::has('admin.password.request'))
                    <a href="{{ route('admin.password.request') }}">{{ __('Forgot your password?') }}</a>
                @endif 
            </div>
            
            <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                
                <x-button class="btn btn-primary py-3 px-4 w-full xl:w-32 xl:mr-3 align-top">
                    {{ __('Login') }}
                </x-button>
                @if(Route::has('admin.register'))
                    <x-button class="btn btn-outline-secondary py-3 px-4 w-full xl:w-32 mt-3 xl:mt-0 align-top">
                        {{ __('Register') }}
                    </x-button>
                @endif
            </div>

        </form>
    </x-auth-card>
</x-guest-layout>
