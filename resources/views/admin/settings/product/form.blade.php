<div>
    {!! Form::label('category_id', 'Network') !!}
    {!! Form::select('category_id', $categories, isset($inputs['category_id']) ? $inputs['category_id'] : null, ['id' => 'category_id',  'class' => $errors->first('category_id') ? ' form-select mt-2 sm:mr-2 border-theme-6' : 'form-select mt-2 sm:mr-2']) !!}
    @error('category_id')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>

<div>
    {!! Form::label('type', 'Type') !!}
    {!! Form::select('type', ['user' => 'User', 'agent' => 'Agent'], isset($inputs['type']) ? $inputs['type'] : null, ['id' => 'type',  'class' => $errors->first('category_id') ? ' form-select mt-2 sm:mr-2 border-theme-6' : 'form-select mt-2 sm:mr-2']) !!}
    @error('type')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>

<div>
    {!! Form::label('name', 'Name', ['class' => 'form-label']) !!}
    {!! Form::text('name', isset($inputs['name']) ? $inputs['name'] : null, ['id' => 'name', 'class' => $errors->first('name') ? ' form-control border-theme-6' : 'form-control', 'placeholder' => 'Enter name']) !!}
    @error('name')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>
<div>
    {!! Form::label('price', 'Price', ['class' => 'form-label']) !!}
    {!! Form::text('price', isset($inputs['price']) ? $inputs['price'] : null, ['id' => 'price', 'class' => $errors->first('price') ? ' form-control border-theme-6' : 'form-control', 'placeholder' => 'Enter price']) !!}
    @error('price')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>

<div>
    {!! Form::label('description', 'Description', ['class' => 'form-label']) !!}
    {!! Form::text('description', isset($inputs['description']) ? $inputs['description'] : null, ['id' => 'description', 'class' => $errors->first('description') ? ' form-control border-theme-6' : 'form-control', 'placeholder' => 'Enter description']) !!}
    @error('description')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>

<div>
    {!! Form::label('validity', 'Validity', ['class' => 'form-label']) !!}
    {!! Form::text('validity', isset($inputs['validity']) ? $inputs['validity'] : null, ['id' => 'validity', 'class' => $errors->first('validity') ? ' form-control border-theme-6' : 'form-control', 'placeholder' => 'Enter validity']) !!}
    @error('validity')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>

<button type="submit" class="btn btn-primary mt-5">{{ $submitButtonText }}</button>