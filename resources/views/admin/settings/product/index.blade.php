<x-app-layout>
    <!-- END: Top Bar -->
                <div class="intro-y flex items-center h-10">
                                    <h2 class="text-lg font-medium truncate mr-5">
                                        Products
                                    </h2>
                                    <a href="{{ route('products.create') }}" class="ml-auto flex items-center text-theme-1 dark:text-theme-10"> <i data-feather="plus" class="w-4 h-4 mr-3"></i> Add New Category </a>
                                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <!-- BEGIN: Data List -->
                    <div class="intro-y col-span-12 lg:col-span-12">
                        <!-- BEGIN: Basic Table -->
                        <div class="intro-y box">
                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200">
                                <h2 class="font-medium text-base mr-auto">
                                    Products
                                </h2>
                            </div>
                            <div class="p-5" id="basic-table">
                                <div class="preview">
                                    <div class="overflow-x-auto">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">#</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Network</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Type</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Name</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Price</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Validity</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Status</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap items-right">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($products as $sn => $product)
                                                    <tr>
                                                        <td class="border-b dark:border-dark-5">{{ $sn + 1 }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ $product->category->name }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ $product->type }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ $product->name }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ $product->price }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ $product->validity }}</td>
                                                        <td class="border-b dark:border-dark-5">
                                                        @if($product->status == 0)
                                                            <span class="text-theme-6">     Inactive 
                                                            </span>
                                                        @else
                                                            <span class="text-theme-9">     Active
                                                            </span>
                                                        @endif
                                                        </td>
                                                        <td class="border-b dark:border-dark-5">
                                                            <div class="flex">
                                                                <a href="{{ url('admin/products/' .$product->id . '/edit') }}" class="flex mr-3">
                                            <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit
                                        </a>
                                                                {!! Form::open(['method' => 'DELETE', 'url' => 'admin/products/' . $product->id]) !!}
                                        
                                        <button type="submit" class="flex text-theme-6" onclick="return confirm('Are you sure?')"><i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete</button>
                                        
                                        {!! Form::close() !!}
                                                        </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <!-- END: Basic Table -->
                        
                    </div>
                   
                </div>            
</x-app-layout>
