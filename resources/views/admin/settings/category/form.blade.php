
<div>
    {!! Form::label('name', 'Name', ['class' => 'form-label']) !!}
    {!! Form::text('name', isset($inputs['name']) ? $inputs['name'] : null, ['id' => 'name', 'class' => $errors->first('name') ? ' form-control border-theme-6' : 'form-control', 'placeholder' => 'Enter name']) !!}
    @error('name')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>

<button type="submit" class="btn btn-primary mt-5">{{ $submitButtonText }}</button>