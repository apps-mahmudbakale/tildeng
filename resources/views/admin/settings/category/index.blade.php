<x-app-layout>
    @push('styles')
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.22/datatables.min.css"/>
    @endpush
    <!-- END: Top Bar -->
                <div class="intro-y flex items-center h-10">
                                    <h2 class="text-lg font-medium truncate mr-5">
                                        Categories
                                    </h2>
                                    <a href="{{ route('categories.create') }}" class="ml-auto flex items-center text-theme-1 dark:text-theme-10"> <i data-feather="plus" class="w-4 h-4 mr-3"></i> Add New Category </a>
                                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <!-- BEGIN: Data List -->
                    <div class="intro-y col-span-12 lg:col-span-12">
                        <!-- BEGIN: Basic Table -->
                        <div class="intro-y box">
                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200">
                                <h2 class="font-medium text-base mr-auto">
                                    Categories
                                </h2>
                            </div>
                            <div class="p-5" id="basic-table">
                                <div class="preview">
                                    <div class="overflow-x-auto">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">#</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Name</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Status</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap items-right">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($categories as $sn => $category)
                                                    <tr>
                                                        <td class="border-b dark:border-dark-5">{{ $sn + 1 }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ $category->name }}</td>
                                                        <td class="border-b dark:border-dark-5">
                                                        @if($category->status == 0)
                                                            <span class="text-theme-6">     Inactive 
                                                            </span>
                                                        @else
                                                            <span class="text-theme-9">     Active
                                                            </span>
                                                        @endif
                                                        </td>
                                                        <td class="border-b dark:border-dark-5">
                                                            <div class="flex">
                                                                <a href="{{ url('categories/' .$category->id . '/edit') }}" class="flex mr-3">
                                            <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit
                                        </a>
                                                                {!! Form::open(['method' => 'DELETE', 'url' => 'admin/categories/' . $category->id]) !!}
                                        
                                        <button type="submit" class="flex text-theme-6" onclick="return confirm('Are you sure?')"><i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete</button>
                                        
                                        {!! Form::close() !!}
                                                        </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <!-- END: Basic Table -->
                        
                    </div>
                   
                </div>
@push('scripts') 
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.22/datatables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endpush                   
</x-app-layout>
