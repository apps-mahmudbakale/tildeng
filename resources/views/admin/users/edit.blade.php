<x-app-layout>
    <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                        Users
                    </h2>
                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="intro-y col-span-12 lg:col-span-6">
                        <!-- BEGIN: Input -->
                        <div class="intro-y box">
                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                                <h2 class="font-medium text-base mr-auto">
                                    Update User
                                </h2>
                                <div class="form-check w-full sm:w-auto sm:ml-auto mt-3 sm:mt-0">
                                    <a href="{{ route('users.index') }}" class="ml-auto flex items-center text-theme-1 dark:text-theme-10"> <i data-feather="list" class="w-4 h-4 mr-3"></i> List Users </a>
                                </div>
                            </div>
                            <div id="input" class="p-5">
                                <div class="preview">
                                    {!! Form::model($user, ['method' => 'PATCH', 'route' => ['users.update', $user->id]]) !!}
                                        @csrf
                                        @include('admin.users.form', ['submitButtonText' => 'Update'])
                                    {!! Form::close() !!}   
                                </div>
                            </div>
                        </div>
                        <!-- END: Input -->
                        
                    </div>
                </div>           
</x-app-layout>
