<x-app-layout>
    <!-- END: Top Bar -->
                <div class="intro-y flex items-center h-10">
                                    <h2 class="text-lg font-medium truncate mr-5">
                                         {{ $type }} Purchases
                                    </h2>
                                    
                                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <!-- BEGIN: Data List -->
                    <div class="intro-y col-span-12 lg:col-span-12">
                        <!-- BEGIN: Basic Table -->
                        <div class="intro-y box">
                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200">
                                <h2 class="font-medium text-base mr-auto">
                                    {{ $type }} Purchases
                                </h2>
                            </div>
                            <div class="p-5" id="basic-table">
                                <div class="preview">
                                    <div class="overflow-x-auto">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">#</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Name</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Phone Number</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Product</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Amount</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Status</th>
                                                    @if($type == 'Approved')
                                                        <th class="border-b-2 dark:border-dark-5 whitespace-nowrap items-right">Approved By</th>
                                                    @else
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap items-right">Action</th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($purchases as $sn => $purchase)
                                                    <tr>
                                                        <td class="border-b dark:border-dark-5">{{ $sn + 1 }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ @$purchase->user->name }}</td>
                                                       
                                                        <td class="border-b dark:border-dark-5">{{ @$purchase->product->category->name . ' ' . @$purchase->product->name }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ number_format(@$purchase->product->price, 2) }}</td>
                                                        <td class="border-b dark:border-dark-5">
                                                        @if($purchase->status == 1)
                                                            <span class="text-theme-6">     Pending 
                                                            </span>
                                                        @else
                                                            <span class="text-theme-9">     Approved
                                                            </span>
                                                        @endif
                                                        </td>
                                                        
                                                        @if($type == 'Approved')
                                                            <td class="border-b dark:border-dark-5">
                                                                <span class="text-theme-9"> {{ $purchase->admin->name }} </span>
                                                            </td>
                                                        @else
                                                            <td class="border-b dark:border-dark-5">
                                                            <a href="{{ url('admin/purchases/approve/' .$purchase->id) }}" class="flex mr-3 btn btn-sm btn-success" onclick="return confirm('Are you sure you want to approve?')">
                                            <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Approve
                                        </a>
                                                        </td>
                                                        @endif
                                                        
                                                    </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <!-- END: Basic Table -->
                        
                    </div>
                   
                </div>            
</x-app-layout>
