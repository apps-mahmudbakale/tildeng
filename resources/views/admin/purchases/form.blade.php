<div>
    {!! Form::label('status', 'Type') !!}
    {!! Form::select('status', [1 => 'Customer', 2 => 'Agent'], isset($inputs['status']) ? $inputs['status'] : null, ['id' => 'status',  'class' => $errors->first('status') ? ' form-select mt-2 sm:mr-2 border-theme-6' : 'form-select mt-2 sm:mr-2']) !!}
    @error('status')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>
<div>
    {!! Form::label('name', 'Name', ['class' => 'form-label']) !!}
    {!! Form::text('name', isset($inputs['name']) ? $inputs['name'] : null, ['id' => 'name', 'class' => $errors->first('name') ? ' form-control border-theme-6' : 'form-control', 'placeholder' => 'Enter name']) !!}
    @error('name')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>
<div>
    {!! Form::label('email', 'Email', ['class' => 'form-label']) !!}
    {!! Form::text('email', isset($inputs['email']) ? $inputs['email'] : null, ['id' => 'email', 'class' => $errors->first('email') ? ' form-control border-theme-6' : 'form-control', 'placeholder' => 'Enter email']) !!}
    @error('email')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>

<div>
    {!! Form::label('phone', 'Phone Number', ['class' => 'form-label']) !!}
    {!! Form::text('phone', isset($inputs['phone']) ? $inputs['phone'] : null, ['id' => 'phone', 'class' => $errors->first('phone') ? ' form-control border-theme-6' : 'form-control', 'placeholder' => 'Enter phone']) !!}
    @error('phone')
        <div class="text-theme-6 mt-2">{{ $message }}</div>
    @enderror
</div>


<button type="submit" class="btn btn-primary mt-5">{{ $submitButtonText }}</button>