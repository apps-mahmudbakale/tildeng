<x-app-layout>
    <!-- END: Top Bar -->
                <div class="intro-y flex items-center h-10">
                                    <h2 class="text-lg font-medium truncate mr-5">
                                        Users
                                    </h2>
                                    
                                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <!-- BEGIN: Data List -->
                    <div class="intro-y col-span-12 lg:col-span-12">
                        <!-- BEGIN: Basic Table -->
                        <div class="intro-y box">
                            <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200">
                                <h2 class="font-medium text-base mr-auto">
                                    Users
                                </h2>
                            </div>
                            <div class="p-5" id="basic-table">
                                <div class="preview">
                                    <div class="overflow-x-auto">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">#</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Name</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Balance</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Phone Number</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap">Created At</th>
                                                    <th class="border-b-2 dark:border-dark-5 whitespace-nowrap items-right">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($users as $sn => $user)
                                                    <tr>
                                                        <td class="border-b dark:border-dark-5">{{ $sn + 1 }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ $user->name }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ $user->balance }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ $user->phone }}</td>
                                                        <td class="border-b dark:border-dark-5">{{ $user->created_at }}</td>
                                                        
                                                        <td class="border-b dark:border-dark-5">
                                                            <div class="flex">
                                                                <a href="{{ url('admin/user/' .$user->id . '/edit') }}" class="flex mr-3">
                                            <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit
                                        </a>
                                                                {!! Form::open(['method' => 'DELETE', 'url' => 'admin/users/' . $user->id]) !!}
                                        
                                        <button type="submit" class="flex text-theme-6" onclick="return confirm('Are you sure?')"><i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete</button>
                                        
                                        {!! Form::close() !!}
                                                        </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <!-- END: Basic Table -->
                        
                    </div>
                   
                </div>            
</x-app-layout>
