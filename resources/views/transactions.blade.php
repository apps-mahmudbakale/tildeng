@extends('layouts.app2')

@section('content')
    <div class="container">
        <div class="row"> 
        <!-- Left Panel
        ============================================= -->
        <aside class="col-lg-3"> 
          
          <!-- Profile Details
          =============================== -->
          <div class="bg-white shadow-sm rounded text-center p-3 mb-4">
            <div class="profile-thumb mt-3 mb-4"> <img class="rounded-circle" src="{{ Auth::user()->image == null ? asset('frontend/images/profile-thumb.jpg') : Auth::user()->image }}" alt="" style="max-width: 100px; max-height: 100px; display: block;">
              <div class="profile-thumb-edit custom-file bg-primary text-white" data-toggle="tooltip" title="Change Profile Picture"> <i class="fas fa-camera position-absolute"></i>
                <input type="file" class="custom-file-input" id="customFile" accept="image/*"/>
              </div>
            </div>
            <p class="text-3 font-weight-500 mb-2">Hello, {{ Auth::user()->name }} ({{ Auth::user()->phone }})</p>
            <p class="text-2">{{ Auth::user()->status == 1 ? 'Customer' : 'Agent' }}</p>
            <p class="mb-2"><a href="{{ url('profile') }}" class="text-5 text-light" data-toggle="tooltip" title="Edit Profile"><i class="fas fa-edit"></i></a></p>
          </div>
          <!-- Profile Details End --> 
          
          <!-- Available Balance
          =============================== -->
          <div class="bg-white shadow-sm rounded text-center p-3 mb-4">
            <div class="text-17 text-light my-3"><i class="fas fa-wallet"></i></div>
            <h3 class="text-9 font-weight-400">₦{{ number_format(Auth::user()->wallet->balance, 2) }}</h3>
            <p class="mb-2 text-muted opacity-8">Available Balance</p>
            <hr class="mx-n3">
            <div class="d-flex"><a href="{{ route('topup') }}" class="btn-link mr-auto">Topup</a> <a href="{{ route('buy') }}" class="btn-link ml-auto">Buy Data</a></div>
          </div>
          <!-- Available Balance End --> 
          
          <!-- Need Help?
          =============================== -->
          <div class="bg-white shadow-sm rounded text-center p-3 mb-4">
            <div class="text-17 text-light my-3"><i class="fas fa-comments"></i></div>
            <h3 class="text-5 font-weight-400 my-4">Need Help?</h3>
            <p class="text-muted opacity-8 mb-4">Have questions or concerns regrading your account?<br>
              Our experts are here to help!.</p>
            <a href="https://wa.me/message/3LC7UFQZ2L2JJ1" target="_blank" class="btn btn-primary btn-block">Chate with Us</a> </div>
          <!-- Need Help? End --> 
          
        </aside>
        <!-- Left Panel End --> 
        
        <!-- Middle Panel
        ============================================= -->
        <div class="col-lg-9">
          <h2 class="font-weight-400 mb-3">Transactions</h2>
          
          <!-- Filter
          ============================================= -->
          {{-- <div class="row">
            <div class="col mb-2">
              <form id="filterTransactions" method="post">
                <div class="form-row"> 
                  <!-- Date Range
                  ========================= -->
                  <div class="col-sm-6 col-md-5 form-group">
                    <input id="dateRange" type="text" class="form-control" placeholder="Date Range">
                    <span class="icon-inside"><i class="fas fa-calendar-alt"></i></span> </div>
                  <!-- All Filters Link
                  ========================= -->
                  <div class="col-auto d-flex align-items-center mr-auto form-group" data-toggle="collapse"> <a class="btn-link" data-toggle="collapse" href="#allFilters" aria-expanded="false" aria-controls="allFilters">All Filters<i class="fas fa-sliders-h text-3 ml-1"></i></a> </div>
                  <!-- Statements Link
                  ========================= -->
                  <div class="col-auto d-flex align-items-center ml-auto form-group">
                    <div class="dropdown"> <a class="text-muted btn-link" href="#" role="button" id="statements" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-file-download text-3 mr-1"></i>Statements</a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="statements"> <a class="dropdown-item" href="#">CSV</a> <a class="dropdown-item" href="#">PDF</a> </div>
                    </div>
                  </div>
                  
                  <!-- All Filters collapse
                  ================================ -->
                  <div class="col-12 collapse mb-3" id="allFilters">
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="allTransactions" name="allFilters" class="custom-control-input" checked>
                      <label class="custom-control-label" for="allTransactions">All Transactions</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="paymentsSend" name="allFilters" class="custom-control-input">
                      <label class="custom-control-label" for="paymentsSend">Payments Send</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="paymentsReceived" name="allFilters" class="custom-control-input">
                      <label class="custom-control-label" for="paymentsReceived">Payments Received</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="refunds" name="allFilters" class="custom-control-input">
                      <label class="custom-control-label" for="refunds">Refunds</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="withdrawal" name="allFilters" class="custom-control-input">
                      <label class="custom-control-label" for="withdrawal">Withdrawal</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="deposit" name="allFilters" class="custom-control-input">
                      <label class="custom-control-label" for="deposit">Deposit</label>
                    </div>
                  </div>
                  <!-- All Filters collapse End --> 
                </div>
              </form>
            </div>
          </div> --}}
          <!-- Filter End --> 
          
          <!-- All Transactions
          ============================================= -->
          <div class="bg-white shadow-sm rounded py-4 mb-4">
            <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">All Transactions</h3>
            <!-- Title
            =============================== -->
            <div class="transaction-title py-2 px-4">
              <div class="row font-weight-00">
                <div class="col-2 col-sm-1 text-center"><span class="">Date</span></div>
                <div class="col col-sm-3">Description</div>
                <div class="col col-sm-4">Phone</div>
                <div class="col-auto col-sm-2 d-none d-sm-block text-center">Status</div>
                <div class="col-3 col-sm-2 text-right">Amount</div>
              </div>
            </div>
            <!-- Title End --> 
            
            <!-- Transaction List
            =============================== -->
            <div class="transaction-list">
                @foreach($transactions as $key => $transaction)
                  <div class="transaction-item px-4 py-3" data-toggle="modal" data-target="#transaction-detail" data-description="Data Purchase" data-network="{{ $transaction->product->category->name }}" data-product="{{ $transaction->product->name }}" data-amount="₦{{ $transaction->product->price }}" data-reference="{{ $transaction->reference_no }}" data-status="{{ $transaction->status == 2 ? 'Approved' : 'In Progress' }}" data-date="{{ Carbon\Carbon::parse($transaction->created_at)->format('d-M-Y') }}" data-paidby="Wallet">
                <div class="row align-items-center flex-row">
                  <div class="col-2 col-sm-1 text-center"> <span class="d-block text-4 font-weight-300">{{ Carbon\Carbon::parse($transaction->created_at)->format('d') }}</span> <span class="d-block text-1 font-weight-300 text-uppercase">{{ Carbon\Carbon::parse($transaction->created_at)->format('M') }}</span> </div>
                  <div class="col col-sm-3"> <span class="d-block text-4">{{ $transaction->product->name }}</span> <span class="text-muted">Data Purchase</span> </div>
                  <div class="col col-sm-4"> <span class="d-block text-4">{{ $transaction->phone }}</span></div>
                   @if($transaction->status == 2)
                    <div class="col-sm-2 text-center text-3"> <span class="text-success" data-toggle="tooltip" data-original-title="Approved"><i class="fas fa-check-circle"></i> Approved</span> </div>
                  @else
                    <div class="col-sm-2 text-center text-3"> <span class="text-danger" data-toggle="tooltip" data-original-title="In Progress"><i class="fas fa-times-circle"></i> In Progress</span> </div>
                  @endif
                  <div class="col-3 col-sm-2 text-right text-4"> <span class="text-nowrap">- ₦{{ $transaction->product->price }}</span> <span class="text-2 text-uppercase">(NGN)</span> </div>
                </div>
              </div>
                @endforeach
            </div>
            <!-- Transaction List End -->  
            
            <!-- Transaction Item Details Modal
            =========================================== -->
            <div id="transaction-detail" class="modal fade" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered transaction-details" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <div class="row no-gutters">
                      <div class="col-sm-5 d-flex justify-content-center bg-primary rounded-left py-4">
                        <div class="my-auto text-center">
                          <div class="text-17 text-white my-3"><i class="fas fa-building"></i></div>
                          <h3 class="text-4 text-white font-weight-400 my-3">Tilde Telecommunication Ltd</h3>
                          <div class="text-8 font-weight-500 text-white my-4" id="amount"></div>
                          <p class="text-white" id="date"></p>
                        </div>
                      </div>
                      <div class="col-sm-7">
                        <h5 class="text-5 font-weight-400 m-3">Transaction Details
                          <button type="button" class="close font-weight-400" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        </h5>
                        <hr>
                        <div class="px-3">
                          <ul class="list-unstyled">
                            <li class="mb-2">Payment Amount <span class="float-right text-3" id="amount">$562.00</span></li>
                            <li class="mb-2">Fee <span class="float-right text-3">-₦0.00</span></li>
                          </ul>
                          <hr class="mb-2">
                          <p class="d-flex align-items-center font-weight-500 mb-4">Total Amount <span class="text-3 ml-auto" id="amount"></span></p>
                          <ul class="list-unstyled">
                            <li class="font-weight-500">Paid By:</li>
                            <li class="text-muted" id="paidby"></li>
                          </ul>
                          <ul class="list-unstyled">
                            <li class="font-weight-500">Transaction ID:</li>
                            <li class="text-muted" id="reference"></li>
                          </ul>
                          <ul class="list-unstyled">
                            <li class="font-weight-500">Description:</li>
                            <li class="text-muted" id="product"></li>
                          </ul>
                          <ul class="list-unstyled">
                            <li class="font-weight-500">Status:</li>
                            <li class="text-muted" id="status"><span class="text-success text-3 ml-1"><i class="fas fa-check-circle"></i></span></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Transaction Item Details Modal End -->  
            
            <!-- Pagination
            ============================================= -->
            {{ $transactions->links() }}
            {{-- <ul class="pagination justify-content-center mt-4 mb-0">
              <li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1"><i class="fas fa-angle-left"></i></a> </li>
              <li class="page-item"><a class="page-link" href="#">1</a></li>
              <li class="page-item active"> <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a> </li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item d-flex align-content-center flex-wrap text-muted text-5 mx-1">......</li>
              <li class="page-item"><a class="page-link" href="#">15</a></li>
              <li class="page-item"> <a class="page-link" href="#"><i class="fas fa-angle-right"></i></a> </li>
            </ul> --}}
            <!-- Paginations end --> 
            
          </div>
          <!-- All Transactions End --> 
        </div>
        <!-- Middle End --> 
      </div>
    </div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
  $(document).ready(function() {
   $(".transaction-item").click(function () {
            var amount = $(this).data('amount');
            var reference = $(this).data('reference');
            var product = $(this).data('product');
            var network = $(this).data('network');
            var date = $(this).data('date');
            var status = $(this).data('status');
            var paidby = $(this).data('paidby');

            $(".modal-body #amount").text(amount);
            $(".modal-body #reference").text(reference);
            $(".modal-body #product").text(network + '/' + product);
            $(".modal-body #date").text(date);
            $(".modal-body #status").text(status);
            $(".modal-body #paidby").text(paidby);
        })
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.rounded-circle').attr('src', e.target.result);
                var formData = new FormData();
                formData.append('fileData', e.target.result);
                 $.ajax({
                  url: '/profile-picture',
                  method: 'post',
                  data: formData,
                  contentType : false,
                  processData : false,
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  success: function(response){
                      console.log(response);
                    }

                });
            }
            reader.readAsDataURL(input.files[0]);


        }
    }
    

    $(".custom-file-input").on('change', function(){
        readURL(this);
    });
    
    $(".position-absolute").on('click', function() {
       $(".custom-file-input").click();
    });
});
</script>