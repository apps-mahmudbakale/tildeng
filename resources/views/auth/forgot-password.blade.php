<x-guest-layout>
    <x-auth-card>
        <x-slot name="header">
            {{ __('Forgot your password?') }}
        </x-slot>

        <x-slot name="info">
            {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="intro-x mt-8">
                <div class="input-form @error('email') has-error @enderror">
                    <x-input id="email" class="intro-x login__input input--lg border border-gray-300 block" type="email" name="email" value="{{ old('email') }}" autofocus placeholder="Email" />
                    @error('email')
                        <div class="pristine-error text-theme-6 mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>    
            </div>
            <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                
                <x-button class="text-white bg-theme-1 xl:mr-3 xl:w-100 align-top">
                    {{ __('Email Password Reset Link') }}
                </x-button>
                
            </div>

        </form>
    </x-auth-card>
</x-guest-layout>
