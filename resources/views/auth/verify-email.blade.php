<x-guest-layout>
    <x-auth-card>
        <x-slot name="header">
            <div class="rounded-md px-5 py-4 mb-2 border text-gray-700 dark:text-gray-300 dark:border-dark-5">
                {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
            </div>
        </x-slot>

        <x-slot name="info">
            {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
        </x-slot>

        @if (session('status') == 'verification-link-sent')
            <div class="rounded-md px-5 py-4 mb-2 bg-theme-9 text-white">
                {{ __('A new verification link has been sent to the email address you provided during registration.') }}
            </div>
        @endif
        

        <form method="POST" action="{{ route('verification.send') }}">
            @csrf

            <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                
                <x-button class="text-white bg-theme-1 xl:mr-3 xl:w-100 align-top">
                    {{ __('Resend Verification Email') }}
                </x-button>
                
            </div>

        </form>

        <form method="POST" action="{{ route('logout') }}">
            @csrf

            <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                
                <x-button class="text-white bg-theme-6 xl:mr-3 xl:w-100 align-top">
                    {{ __('Logout') }}
                </x-button>
                
            </div>

        </form>
    </x-auth-card>
</x-guest-layout>
