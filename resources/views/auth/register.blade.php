@extends('layouts.auth')

@section('content')
    <div class="col-md-6"> 
        <!-- Get Verified! Text
        ============================================= -->
        <div class="hero-wrap d-flex align-items-center h-100">
          <div class="hero-mask opacity-8 bg-primary"></div>
          <div class="hero-bg hero-bg-scroll" style="background-image:url('frontend/images/bg/image-3.jpg');"></div>
          <div class="hero-content mx-auto w-100 h-100 d-flex flex-column">
            <div class="row  no-gutters">
              <div class="col-10 col-lg-9 mx-auto">
                <div class="logo mt-5 mb-5 mb-md-0"> <a class="d-flex" href="{{ url('/') }}" title="Payyed - HTML Template"><img src="frontend/images/logo-light.png" alt="Payyed"></a> </div>
              </div>
            </div>
            <div class="row my-auto">
              <div class="col-10 col-lg-9 mx-auto">
                <h1 class="text-11 text-white mb-4">{{ __('Create an account') }}</h1>
                <p class="text-4 text-white line-height-4 mb-5">Every day, T&T makes thousands of customers happy.</p>
              </div>
            </div>
          </div>
        </div>
        <!-- Get Verified! Text End --> 
      </div>
      <div class="col-md-6 d-flex align-items-center"> 
        <!-- SignUp Form
        ============================================= -->
        <div class="container my-4">
          <div class="row">
            <div class="col-11 col-lg-9 col-xl-8 mx-auto">
              <h3 class="font-weight-400 mb-4">Sign Up</h3>
              <form id="loginForm" method="post" action="{{ route('register') }}">
                @csrf
                <div class="form-group">
                  <label for="fullName">Full Name</label>
                  <input type="text" class="form-control" name="name" id="fullName" required placeholder="Enter Your Name">
                    @error('name')
                        <div class="text-danger text-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="phone">Phone</label>
                  <input type="text" class="form-control" name="phone" id="phone" required placeholder="Enter Your Mobile Phone">
                    @error('phone')
                        <div class="text-danger text-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="emailAddress">Email Address</label>
                  <input type="email" class="form-control" name="email" id="emailAddress" required placeholder="Enter Your Email">
                    @error('email')
                        <div class="text-danger text-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="loginPassword">Password</label>
                  <input type="password" name="password" class="form-control" id="loginPassword" required placeholder="Enter Password">
                    @error('password')
                        <div class="text-danger text-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="loginPassword">Password</label>
                  <input type="password" name="password_confirmation" class="form-control" id="loginPassword" required placeholder="Confirm Password">
                    @error('password_confirmation')
                        <div class="text-danger text-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button class="btn btn-primary btn-block my-4" type="submit">Sign Up</button>
              </form>
              <p class="text-3 text-center text-muted">Already have an account? <a class="btn-link" href="{{ route('login') }}">Log In</a></p>
            </div>
          </div>
        </div>
        <!-- SignUp Form End --> 
      </div>
@endsection
