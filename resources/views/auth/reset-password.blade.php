<x-guest-layout>
    <x-auth-card>
        <x-slot name="header">
            {{ __('Reset Password') }}
        </x-slot>

        <x-slot name="info">
            {{ __('Reset Password') }}
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <div class="intro-x mt-8">
                <!-- Password Reset Token -->
                <input type="hidden" name="token" value="{{ $request->route('token') }}">

                <div class="input-form @error('email') has-error @enderror">
                    <x-input 
                        id="email" 
                        class="intro-x login__input input--lg border border-gray-300 block" 
                        type="email" 
                        name="email" 
                        value="{{ old('email') }}" 
                        autofocus 
                        placeholder="Email" />
                    @error('email')
                        <div class="pristine-error text-theme-6 mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="input-form @error('password') has-error @enderror">
                    <x-input 
                        id="password" 
                        class="intro-x login__input input--lg border border-gray-300 block" 
                        type="password" 
                        name="password" 
                        placeholder="Password" 
                        autocomplete="current-password" />
                    @error('password')
                        <div class="pristine-error text-theme-6 mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="input-form @error('password_confirmation') has-error @enderror">
                    <x-input 
                        id="password_confirmation" 
                        class="intro-x login__input input--lg border border-gray-300 block" 
                        type="password" 
                        name="password_confirmation" 
                        placeholder="Confirm Password" />
                    @error('password_confirmation')
                        <div class="pristine-error text-theme-6 mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>  


            </div>
            <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                
                <x-button class="text-white bg-theme-1 xl:mr-3 xl:w-100 align-top">
                    {{ __('Reset Password') }}
                </x-button>
                
            </div>

        </form>
    </x-auth-card>
</x-guest-layout>
