<x-guest-layout>
    <x-auth-card>
        <x-slot name="header">
            {{ __('Confirm Your Password') }}
        </x-slot>

        <x-slot name="info">
            This is a secure area of the application. Please confirm your password before continuing.
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('password.confirm') }}">
            @csrf

            <div class="intro-x mt-8">
                <div class="input-form @error('password') has-error @enderror">
                    <x-input 
                        id="password" 
                        class="intro-x login__input input--lg border border-gray-300 block" 
                        type="password" 
                        name="password" 
                        placeholder="Password" 
                        autocomplete="current-password" />
                    @error('password')
                        <div class="pristine-error text-theme-6 mt-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>    
            </div>
            <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                
                <x-button class="text-white bg-theme-1 xl:mr-3 xl:w-100 align-top">
                    {{ __('Confirm') }}
                </x-button>
                
            </div>

        </form>
    </x-auth-card>
</x-guest-layout>
