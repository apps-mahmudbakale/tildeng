@extends('layouts.auth')

@section('content')
      <!-- Welcome Text
      ============================================= -->
      <div class="col-md-6">
        <div class="hero-wrap d-flex align-items-center h-100">
          <div class="hero-mask opacity-8 bg-primary"></div>
          <div class="hero-bg hero-bg-scroll" style="background-image:url('frontend/images/bg/image-3.jpg');"></div>
          <div class="hero-content mx-auto w-100 h-100 d-flex flex-column">
            <div class="row no-gutters">
              <div class="col-10 col-lg-9 mx-auto">
                <div class="logo mt-5 mb-5 mb-md-0"> <a class="d-flex" href="index.html" title="Payyed - HTML Template"><img src="frontend/images/logo-light.png" alt="Payyed"></a> </div>
              </div>
            </div>
            <div class="row no-gutters my-auto">
              <div class="col-10 col-lg-9 mx-auto">
                <h1 class="text-11 text-white mb-4">Welcome back!</h1>
                <p class="text-4 text-white line-height-4 mb-5">We are glad to see you again! Instant deposits, withdrawals & payouts trusted by millions worldwide.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Welcome Text End --> 
      
      <!-- Login Form
      ============================================= -->
      <div class="col-md-6 d-flex align-items-center">
        <div class="container my-4">
          <div class="row">
            <div class="col-11 col-lg-9 col-xl-8 mx-auto">
              <h3 class="font-weight-400 mb-4">Log In</h3>
              <form id="loginForm" method="post" action="{{ route('login') }}">
                @csrf
                <div class="form-group @error('email') has-error @enderror">
                  <label for="emailAddress">Email Address</label>
                  <input type="email" name="email" class="form-control" id="emailAddress" required placeholder="Enter Your Email" value="{{ old('email') }}">

                    @error('email')
                        <div class="text-danger text-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group @error('password') has-error @enderror">
                  <label for="loginPassword">Password</label>
                  <input type="password" name="password" class="form-control" id="loginPassword" required placeholder="Enter Password">

                    @error('password')
                        <div class="text-danger text-2">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="row">
                  <div class="col-sm">
                    <div class="form-check custom-control custom-checkbox">
                      <input id="remember-me" name="remember" class="custom-control-input" type="checkbox">
                      <label class="custom-control-label" for="remember-me">Remember Me</label>
                    </div>
                  </div>
                  @if (Route::has('password.request'))
                    <div class="col-sm text-right"><a class="btn-link" href="{{ route('password.request') }}">Forgot Password ?</a></div>
                @endif
                </div>
                <button class="btn btn-primary btn-block my-4" type="submit">Login</button>
              </form>
              @if(Route::has('register'))
                <p class="text-3 text-center text-muted">Don't have an account? <a class="btn-link" href="{{ route('register') }}">Sign Up</a></p>
            @endif
            </div>
          </div>
        </div>
      </div>
      <!-- Login Form End --> 
@endsection