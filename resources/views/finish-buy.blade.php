@extends('layouts.app2')

@section('secondary-menu')
  <!-- Secondary menu
  ============================================= -->
  <div class="bg-white">
    <div class="container d-flex justify-content-center">
      <ul class="nav secondary-nav alternate">
        <li class="nav-item"> <a class="nav-link" href="{{ route('topup') }}">Topup</a></li>
        <li class="nav-item"> <a class="nav-link active" href="{{ route('buy') }}">Buy Data</a></li>
      </ul>
    </div>
  </div>
  <!-- Secondary menu end --> 
@stop

@section('content')
    <div class="container"> 
      
      <!-- Steps Progress bar -->
      <div class="row mt-4 mb-5">
        <div class="col-lg-11 mx-auto">
          <div class="row widget-steps">
            <div class="col-4 step complete">
              <div class="step-name">Details</div>
              <div class="progress">
                <div class="progress-bar"></div>
              </div>
              <span class="step-dot"></span> </div>
            <div class="col-4 step complete">
              <div class="step-name">Confirm</div>
              <div class="progress">
                <div class="progress-bar"></div>
              </div>
              <span class="step-dot"></span> </div>
            <div class="col-4 step complete">
              <div class="step-name">Success</div>
              <div class="progress">
                <div class="progress-bar"></div>
              </div>
              <span class="step-dot"></span> </div>
          </div>
        </div>
      </div>
      <h2 class="font-weight-400 text-center mt-3 mb-4">Buy Data</h2>
      <div class="row">
        <div class="col-md-9 col-lg-7 col-xl-6 mx-auto"> 
          @if($status == "success")
            <!-- Request Money Success
          ============================================= -->
            <div class="bg-white text-center shadow-sm rounded p-3 pt-sm-4 pb-sm-5 px-sm-5 mb-4">
              <div class="my-4">
                <p class="text-success text-20 line-height-07"><i class="fas fa-check-circle"></i></p>
                <p class="text-success text-8 font-weight-500 line-height-07">Success!</p>
                <p class="lead">Reference No: <b>{{ $reference_no }}</b></p>
              </div>
              <p class="text-3 mb-4">You've successfully purchased <span class="text-4 font-weight-500">{{ $product }}</span> to <span class="text-4 font-weight-500">{{ $phone }}</span>  and <span class="text-4 font-weight-500">₦{{ number_format($price, 2) }}</span> Debited from your wallet, See transaction details under <a class="btn-link" href="{{ url('/transactions') }}">Activity</a>.</p>
              <a href="{{ url('dashboard') }}" class="btn btn-primary btn-block" role="button">Return to Dashboard</a>
            <!-- Request Money Success end --> 
          @else
              <!-- Request Money Success
          ============================================= -->
          <div class="bg-white text-center shadow-sm rounded p-3 pt-sm-4 pb-sm-5 px-sm-5 mb-4">
            <div class="my-4">
              <p class="text-danger text-20 line-height-07"><i class="fas fa-times"></i></p>
              <p class="text-danger text-8 font-weight-500 line-height-07">Error!</p>
              <p class="lead">Transactions Failed</p>
            </div>
            <p class="text-3 mb-4">{{ $message }}</a>.</p>
            
            <a class="btn btn-primary btn-block" href="{{ route('buy') }}">Try Again</a>
            <a class="text-3 d-inline-block btn-link mt-4" href="{{ url('/dashboard') }}"><i class="fas fa-home"></i> Return home</a> </div>
          <!-- Request Money Success end --> 
          @endif
        </div>
      </div>
    </div>
@endsection