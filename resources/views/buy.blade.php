@extends('layouts.app2')

@section('secondary-menu')
  <!-- Secondary menu
  ============================================= -->
  <div class="bg-white">
    <div class="container d-flex justify-content-center">
      <ul class="nav secondary-nav alternate">
          <li class="nav-item"> <a class="nav-link" href="{{ route('dashboard') }}">Dashboard</a></li>
        <li class="nav-item"> <a class="nav-link" href="{{ route('topup') }}">Topup</a></li>
        <li class="nav-item"> <a class="nav-link active" href="{{ route('buy') }}">Buy Data</a></li>
        
      </ul>
    </div>
  </div>
  <!-- Secondary menu end --> 
@stop

@section('content')
    <div class="container"> 
      
      <!-- Steps Progress bar -->
      <div class="row mt-4 mb-5">
        <div class="col-lg-11 mx-auto">
          <div class="row widget-steps">
            <div class="col-4 step active">
              <div class="step-name">Details</div>
              <div class="progress">
                <div class="progress-bar"></div>
              </div>
              <a href="#" class="step-dot"></a> </div>
            <div class="col-4 step disabled">
              <div class="step-name">Confirm</div>
              <div class="progress">
                <div class="progress-bar"></div>
              </div>
              <a href="#" class="step-dot"></a> </div>
            <div class="col-4 step disabled">
              <div class="step-name">Success</div>
              <div class="progress">
                <div class="progress-bar"></div>
              </div>
              <a href="#" class="step-dot"></a> </div>
          </div>
        </div>
      </div>
      <h2 class="font-weight-400 text-center mt-3 mb-4">Buy Data</h2>
      <div class="row">
        <div class="col-md-9 col-lg-7 col-xl-6 mx-auto"> 
          <!-- Withdraw Money Form
        ============================================= -->
          <div class="bg-white shadow-sm rounded p-3 pt-sm-5 pb-sm-5 px-sm-5 mb-4">
            <div class="text-center bg-primary p-4 rounded mb-4">
              <h3 class="text-10 text-white font-weight-400">₦{{ number_format(Auth::user()->balance, 2) }}</h3>
              <p class="text-white">Available Balance</p>
              <a href="" class="btn btn-outline-light btn-sm shadow-none text-uppercase rounded-pill text-1">Buy Full Amount of Data</a> </div>
            <form id="form-send-money" method="post" action="{{ route('buy') }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="withdrawto">Network</label>
                <select id="withdrawto" class="custom-select" required="" name="category_id">
                  <option value="">Select Network</option>
                  @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="withdrawto">Product</label>
                <select id="withdrawto" class="custom-select" required="" name="product_id">
                  <option value="">Select Network First</option>
                 {{--  @foreach($products as $product)
                    <option value="{{ $product->id }}">{{ $product->name }} - {{ $product->price }}</option>
                  @endforeach --}}
                </select>
              </div>
              <div class="form-group">
                <label for="youSend">Amount</label>
                <div class="input-group">
                  <div class="input-group-prepend"> <span class="input-group-text">₦</span> </div>
                  <input type="text" class="form-control" data-bv-field="youSend" id="price" value="" placeholder="0" name="price" readonly>
                </div>
              </div>
              <div class="form-group">
                <label for="youSend">Phone Number</label>
                <div class="input-group">
                  <input type="text" class="form-control" data-bv-field="youSend" id="youSend" name="phone"  placeholder="">
                </div>
              </div>
              <p class="text-muted mt-4">Transactions fees <span class="float-right d-flex align-items-center">0.00 NGN</span></p>
              <hr>
              <p class="text-3 font-weight-500">Total Amount <span class="float-right" id="total"></span></p>
              <button class="btn btn-primary btn-block">Continue</button>
              <input type="hidden" name="network" value="">
              <input type="hidden" name="product" value="">
              <input type="hidden" name="validity" value="">
            </form>
          </div>
          <!-- Withdraw Money Form end --> 
        </div>
      </div>
    </div>
    
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
    $("document").ready(function () {
        $('select[name="category_id"]').on('change', function () {
            var catId = $(this).val();
            if (catId) {
                $.ajax({
                    url: '/fetch-products/' + catId,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('select[name="product_id"]').empty();
                        $('select[name="product_id"]').append('<option value="">Select Product</option>');
                        $.each(data, function (key, value) {
                            $('select[name="product_id"]').append('<option value=" ' + value.id + '">' + value.name + ' (' + value.validity + ')' + '</option>');
                        })
                    }

                })
            } else {
                $('select[name="product_id"]').empty();
            }
        });

        $('select[name="product_id"]').on('change', function () {
            var productId = $(this).val();
            console.log(productId);
            if (productId) {
                $.ajax({
                    url: '/fetch-price/' + productId,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('input[name="price"]').empty();
                        $('input[name="price"]').val(data.price);
                        $('#total').text(data.price + ' NGN');
                        $('input[name="product"]').val(data.name);
                        $('input[name="validity"]').val(data.validity);
                        $('input[name="network"]').val(data.category.name);
                    }

                })
            } else {
                $('input[name="price"]').empty();
                $('#total').empty();
                $('input[name="product"]').empty();
                $('input[name="validity"]').empty();
                $('input[name="network"]').empty();
            }
        });


    });
</script>