@extends('layouts.app2')

@section('secondary-menu')
  <!-- Secondary menu
  ============================================= -->
  <div class="bg-white">
    <div class="container d-flex justify-content-center">
      <ul class="nav secondary-nav alternate">
        <li class="nav-item"> <a class="nav-link" href="{{ route('topup') }}">Topup</a></li>
        <li class="nav-item"> <a class="nav-link active" href="{{ route('buy') }}">Buy Data</a></li>
      </ul>
    </div>
  </div>
  <!-- Secondary menu end --> 
@stop

@section('content')
    <!-- Content
  ============================================= -->
  <div class="container"> 
    
    <!-- Steps Progress bar -->
    <div class="row mt-4 mb-5">
      <div class="col-lg-11 mx-auto">
        <div class="row widget-steps">
          <div class="col-4 step complete">
            <div class="step-name">Details</div>
            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <a href="deposit-money.html" class="step-dot"></a> </div>
          <div class="col-4 step active">
            <div class="step-name">Confirm</div>
            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <a href="#" class="step-dot"></a> </div>
          <div class="col-4 step disabled">
            <div class="step-name">Success</div>
            <div class="progress">
              <div class="progress-bar"></div>
            </div>
            <a href="#" class="step-dot"></a> </div>
        </div>
      </div>
    </div>
    <h2 class="font-weight-400 text-center mt-3 mb-4">Buy Data</h2>
    <div class="row">
      <div class="col-md-9 col-lg-7 col-xl-6 mx-auto">
        <div class="bg-white shadow-sm rounded p-3 pt-sm-4 pb-sm-5 px-sm-5 mb-4">
          <h3 class="text-5 font-weight-400 mb-3 mb-sm-4">Buy Data via</h3>
          <hr class="mx-n3 mx-sm-n5 mb-4">
          <!-- Deposit Money Confirm
          ============================================= -->
          <form id="form-send-money" method="post" action="{{ route('confirm') }}">
            {{ csrf_field() }}
            <div class="alert alert-info rounded shadow-sm py-3 px-4 px-sm-2 mb-5">
              <div class="form-row align-items-center">
                <p class="col-sm-5 opacity-7 text-sm-right mb-0 mb-sm-3">Network:</p>
                <p class="col-sm-7 text-3">{{ $network }}</p>
              </div>
              <div class="form-row align-items-center">
                <p class="col-sm-5 opacity-7 text-sm-right mb-0 mb-sm-3">Product:</p>
                <p class="col-sm-7 text-3">{{ $product }}/{{ $validity }}</p>
              </div>
              <div class="form-row align-items-center">
                <p class="col-sm-5 opacity-7 text-sm-right mb-0">Phone:</p>
                <p class="col-sm-7 text-3 mb-0">{{ $phone }}</p>
              </div>
            </div>
            <hr class="mx-n3 mx-sm-n5 mb-4">
            <h3 class="text-5 font-weight-400 mb-4">Details</h3>
            <hr class="mx-n3 mx-sm-n5 mb-4">
            <p class="mb-1">Deposit Amount <span class="text-3 float-right">{{ number_format($price, 2) }} NGN</span></p>
            <p class="mb-1">Fees <span class="text-3 float-right">0.00 NGN</span></p>
            <hr>
            <p class="text-4 font-weight-500">Total<span class="float-right">{{ number_format($price, 2) }} NGN</span></p>
            <button class="btn btn-primary btn-block">Confirm</button>
          </form>
          <!-- Deposit Money Confirm end --> 
        </div>
      </div>
    </div>
  </div>
  <!-- Content end --> 
@endsection