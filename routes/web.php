<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\WebHookController;
use App\Http\Controllers\Api\InboxesController;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('inbox', [InboxesController::class, 'handle']);

Route::get('/is_curl', function() {
   function file_get_contents_with_curl($url)
    {
      $ch = curl_init();
      $timeout = 5;
      curl_setopt($ch,CURLOPT_URL,$url);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
      $data = curl_exec($ch);
      curl_close($ch);
      return $data;
    }
    
    $contents = file_get_contents_with_curl("http://www.husmodata.com/api/data/");
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/testapi', function () {
    return view('test');
});


Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/dashboard/{token}', 'HomeController@show')->name('show');

Route::get('/profile', 'HomeController@profile')->name('profile');
Route::get('/transactions', 'TransactionController@index')->name('transactions');
Route::get('/topups', 'TransactionController@topup')->name('topups');
Route::post('profile-picture', 'ProfileController@profilePicture');
Route::get('/reset-password', 'HomeController@resetPassword')->name('reset-password');
// Route::get('/history', 'HistoryController@index')->name('history');
Route::get('/topup', 'TopupController@index')->name('topup');
Route::get('/buy', 'BuyController@index')->name('buys');
Route::post('/buy', 'BuyController@buy')->name('buy');
Route::get('/confirm', 'BuyController@getConfirm')->name('confirms');
Route::post('/confirm', 'BuyController@postConfirm')->name('confirm');
Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');
Route::get('FundWallet/{data}', 'PaymentController@FundWallet')->name('fund');
Route::get('/callback', 'PaymentController@handleGatewayCallback')->name('callback');
Route::get('/finish', 'PaymentController@finish')->name('finish');
Route::get('/fetch-products/{id}', 'BuyController@fetchProducts');
Route::get('/fetch-price/{id}', 'BuyController@fetchPrice');

Route::namespace("Admin")->prefix('admin')->group(function(){
	Route::get('/dashboard', 'HomeController@dashboard')->name('admin.dashboard');
	Route::get('/profile', 'HomeController@profile')->name('admin.profile');
	Route::get('/reset-password', 'HomeController@resetPassword')->name('admin.reset-password');
	Route::get('/agents', 'HomeController@agents')->name('admin.agents');
	Route::get('purchases/approve/{id}', 'PurchaseController@approve');
	Route::resource('purchases', 'PurchaseController');
	Route::resource('categories', 'CategoryController');
	Route::resource('products', 'ProductController');
	Route::resource('users', UserController::class)->except(['create', 'store']);
	Route::resource('agents', 'AgentController')->except(['create', 'store']);
});

require __DIR__.'/auth.php';
