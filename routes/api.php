<?php

use App\Http\Controllers\Api\WebHookController;
use App\Http\Controllers\Api\InboxesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('inbox', [InboxesController::class, 'handle']);

Route::post('webhooks', [WebHookController::class, 'store']);



//Route::get('webhook', [WebHookController::class, 'index']);
Route::post('webhook', [InboxesController::class, 'handle']);
Route::get('sendpuls/{id}', [WebHookController::class, 'getCustomer']);
Route::post('test', [WebHookController::class, 'reserveAccount']);
Route::post('send-pulse-webhook', [WebHookController::class, 'sendpulseWebhook']);
//Route::post('inbox', [InboxesController::class, 'handle']);