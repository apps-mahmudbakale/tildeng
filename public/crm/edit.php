<?php
	session_start();
	include_once('connection.php');

	if(isset($_POST['edit'])){
		$id = $_POST['id'];
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$phone = $_POST['phone'];
		$tracking = $_POST['tracking'];
		$comment = $_POST['comment'];
		$staff = $_POST['staff'];
		$reply = $_POST['reply'];
		$amount = $_POST['amount'];
		$sql = "UPDATE members SET firstname = '$firstname', lastname = '$lastname',  phone = '$phone', tracking = '$tracking', comment ='$comment', staff='$staff', amount = '$amount', reply = '$reply' WHERE id = '$id'";

		//use for MySQLi OOP
		if($conn->query($sql)){
			$_SESSION['success'] = 'Details updated successfully';
		}
		///////////////

		//use for MySQLi Procedural
		// if(mysqli_query($conn, $sql)){
		// 	$_SESSION['success'] = 'Member updated successfully';
		// }
		///////////////
		
		else{
			$_SESSION['error'] = 'Something went wrong in updating member';
		}
	}
	else{
		$_SESSION['error'] = 'Select member to edit first';
	}

	header('location: adminaccess.php');

?>