<?php
	session_start();
	include_once('connection.php');

	if(isset($_POST['add'])){
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$address = $_POST['address'];
		$phone = $_POST['phone'];
		$tracking = $_POST['tracking'];
		$task = $_POST['task'];
		$comment = $_POST['comment'];
		$staff = $_POST['staff'];
		$reply = $_POST['reply'];
		$amount = $_POST['amount'];
		$sql = "INSERT INTO members (firstname, lastname, phone, address, tracking, task, comment, staff, reply, amount) VALUES ('$firstname', '$lastname','$phone', '$address', '$tracking', '$task', '$comment', '$staff', '$reply', '$amount' )";

		//use for MySQLi OOP
		if($conn->query($sql)){
			$_SESSION['success'] = 'Details added successfully';
		}
		///////////////

		//use for MySQLi Procedural
		// if(mysqli_query($conn, $sql)){
		// 	$_SESSION['success'] = 'Member added successfully';
		// }
		//////////////
		
		else{
			$_SESSION['error'] = 'Something went wrong while adding';
		}
	}
	else{
		$_SESSION['error'] = 'Fill up add form first';
	}

	header('location: index.php');
?>