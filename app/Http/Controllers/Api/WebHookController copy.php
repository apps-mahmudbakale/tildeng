<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WebHook;
use App\Services\Send;
use PDF;

class WebHookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hooks = WebHook::all();
        return $hooks;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function sendPulse($reference, $message)
    {
        $clientId = "ac00d5514972d6da98eb4083eed61b66";
        $clientSecret = "b344d83c7be072c0714e809ea829312b";
        $tokenEndpoint = "https://api.sendpulse.com/oauth/access_token";

        $data = array(
            'grant_type' => 'client_credentials',
            'client_id' => $clientId,
            'client_secret' => $clientSecret
        );

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context  = stream_context_create($options);
        $response = file_get_contents($tokenEndpoint, false, $context);

        if ($response === false) {
            // handle error
            exit("Failed to connect to the authorization server");
        }

        $responseData = json_decode($response, true);

        if (!isset($responseData['access_token'])) {
            // handle error
            exit("Failed to retrieve access token");
        }

        $accessToken = $responseData['access_token'];

        $data = array(
            'contact_id' => $reference,
            'message' => array(
                'type' => 'text',
                'text' => array(
                    'body' => $message
                )
            )
        );

        $send = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/send', $data, $accessToken);

        return  response()->json(['token' => $accessToken]);
    }

    public function getCustomer($id)
    {

        $clientId = "ac00d5514972d6da98eb4083eed61b66";
        $clientSecret = "b344d83c7be072c0714e809ea829312b";
        $tokenEndpoint = "https://api.sendpulse.com/oauth/access_token";

        $data = array(
            'grant_type' => 'client_credentials',
            'client_id' => $clientId,
            'client_secret' => $clientSecret
        );

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context  = stream_context_create($options);
        $response = file_get_contents($tokenEndpoint, false, $context);

        if ($response === false) {
          $clientId = "ac00d5514972d6da98eb4083eed61b66";
        $clientSecret = "b344d83c7be072c0714e809ea829312b";
        $tokenEndpoint = "https://api.sendpulse.com/oauth/access_token";

        $data = array(
            'grant_type' => 'client_credentials',
            'client_id' => $clientId,
            'client_secret' => $clientSecret
        );

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context  = stream_context_create($options);
        $response = file_get_contents($tokenEndpoint, false, $context);
        }

        $responseData = json_decode($response, true);

        if (!isset($responseData['access_token'])) {
            // handle error
            exit("Failed to retrieve access token");
        }

        $accessToken = $responseData['access_token'];

        $send = Send::curlGet('https://api.sendpulse.com/whatsapp/contacts/get?id=' . $id, $accessToken);

        return $send['data'];
    }
    

    public function updateWallet($reference, $amount)
    {
        $clientId = "ac00d5514972d6da98eb4083eed61b66";
        $clientSecret = "b344d83c7be072c0714e809ea829312b";
        $tokenEndpoint = "https://api.sendpulse.com/oauth/access_token";

        $data = array(
            'grant_type' => 'client_credentials',
            'client_id' => $clientId,
            'client_secret' => $clientSecret
        );

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context  = stream_context_create($options);
        $response = file_get_contents($tokenEndpoint, false, $context);

        if ($response === false) {
            // handle error
            exit("Failed to connect to the authorization server");
        }

        $responseData = json_decode($response, true);

        if (!isset($responseData['access_token'])) {
            // handle error
            exit("Failed to retrieve access token");
        }

        $accessToken = $responseData['access_token'];

        $customer = $this->getCustomer($reference);
        $current_balance = $customer['variables']['wal'];
        $data = array(
            'contact_id' => $reference,
            'variable_name' => 'wal',
            'variable_value' => $current_balance + $amount
        );

        $send = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data, $accessToken);
    }



    public function reserveAccount($datas)
    {
        $clientKey = "MK_PROD_VNKFUCQ916";
        $clientSecret = "RNNKW5G9742DU012YTZV5C42Y9T10847";
        $tokenEndpoint = "https://api.monnify.com/api/v1/auth/login";

        $data = base64_encode($clientKey . ':' . $clientSecret);


        $response = Send::curlGet2($tokenEndpoint, $data);

        $token = $response['responseBody']['accessToken'];

        $endpoint = 'https://api.monnify.com/api/v2/bank-transfer/reserved-accounts';

        $send = Send::curlPost($endpoint, $datas, $token);

        return $send;
    }


    public function sendFlow($data)
    {
        $clientId = "ac00d5514972d6da98eb4083eed61b66";
        $clientSecret = "b344d83c7be072c0714e809ea829312b";
        $tokenEndpoint = "https://api.sendpulse.com/oauth/access_token";

        $datax = array(
            'grant_type' => 'client_credentials',
            'client_id' => $clientId,
            'client_secret' => $clientSecret
        );

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($datax),
            ),
        );
        $context  = stream_context_create($options);
        $response = file_get_contents($tokenEndpoint, false, $context);

        if ($response === false) {
            // handle error
            exit("Failed to connect to the authorization server");
        }

        $responseData = json_decode($response, true);

        if (!isset($responseData['access_token'])) {
            // handle error
            exit("Failed to retrieve access token");
        }

        $accessToken = $responseData['access_token'];


        $send = Send::curlPost('https://api.sendpulse.com/whatsapp/flows/run', $data, $accessToken);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ref = $request['eventData']['product']['reference'];
        $sender_name = $request['eventData']['paymentSourceInformation'][0]['accountName'];
        $amount_paid = $request['eventData']['paymentSourceInformation'][0]['amountPaid'];
        $account_num = $request['eventData']['paymentSourceInformation'][0]['accountNumber'];
        $name = $request['eventData']['customer']['name'];
        $email = $request['eventData']['customer']['email'];

        $webhook = WebHook::create([
            'reference' => $ref,
            'account_name' => $sender_name,
            'amount_paid' => $amount_paid,
            'account_number' => $account_num,
            'name' => $name,
            'email' => $email
        ]);
        $message = 'Hello, We have received your payment of N' . $amount_paid . ' Sender Details Name: ' . $sender_name . '  Account Number: ' . $account_num . '  Thank you for choosing Tilde Telecommunications. Reply with Hi to continue ';

        $this->sendPulse($ref, $message);
        if ($amount_paid == '261') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f1183017c6e87ccb51b288', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        } elseif ($amount_paid == '520') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f13048d77c2c4e5f2dc5aa', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        } elseif ($amount_paid == '770') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f1309a5251e370160e72c0', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        } elseif ($amount_paid == '1020') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f131db9c6390009906105f', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        } elseif ($amount_paid == '1220') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f1320d1a914843aa3af7f6', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        } elseif ($amount_paid == '2420') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f1325348c9a5130416778b', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        } elseif ($amount_paid == '255') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f144f2f0df956da3583fb7', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        }elseif ($amount_paid == '515') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f145a0cdb9c60c8a3e1c23', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        }elseif ($amount_paid == '765') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f145c18bdb781d5e5f1437', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        }elseif ($amount_paid == '1015') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f145e19a68086dfd26c8a8', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        }elseif ($amount_paid == '1215') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f145fe1093795e665440f0', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        }elseif ($amount_paid == '2415') {
            $data = array('contact_id' => $ref, 'flow_id' => '63f1462c9a68086dfd26c8aa', 'external_data' => array('tracking_number' => '1234-0987-5678-9012'));
            $this->sendFlow($data);
        }
        else {
            $this->updateWallet($ref, $amount_paid);
        }

        // dd($request['eventData']['customer']['name']);
        return response()->json(['success' => 'Thank You']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sendpulseWebhook(Request $request)
    {
        //return $request[0];
        
        $req = $request[0];
       /// return $req['contact']['id'];


        if ($req['title'] == 'new_subscriber') {
            $name = $req['contact']['name'];
            $ref = $req['contact']['id'];
            $phone = '0' . ltrim($req['contact']['phone'], '234');
            $bank = true;

            $data = array(
                'accountReference' => $ref,
                'accountName' => $name,
                'currencyCode' => 'NGN',
                'contractCode' => '719405548416',
                'customerEmail' => $ref . '@tilde.ng',
                'customerBvn' => '21212121212',
                'customerName' => $name,
                'getAllAvailableBanks' => false,
                'preferredBanks' => array('232', '035')

            );
            //return $data;
            $clientId = "ac00d5514972d6da98eb4083eed61b66";
            $clientSecret = "b344d83c7be072c0714e809ea829312b";
            $tokenEndpoint = "https://api.sendpulse.com/oauth/access_token";

            $datax = array(
                'grant_type' => 'client_credentials',
                'client_id' => $clientId,
                'client_secret' => $clientSecret
            );

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($datax),
                ),
            );
            $context  = stream_context_create($options);
            $response = file_get_contents($tokenEndpoint, false, $context);

            if ($response === false) {
                // handle error
                exit("Failed to connect to the authorization server");
            }

            $responseData = json_decode($response, true);

            if (!isset($responseData['access_token'])) {
                // handle error
                exit("Failed to retrieve access token");
            }

            $accessToken = $responseData['access_token'];

            $customer = $this->getCustomer($ref);
            $current_balance = $customer['variables']['wal'];
            $dataz = array(
                'contact_id' => $ref,
                'variable_name' => 'whatsapp',
                'variable_value' => $phone
            );

            $send = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $dataz, $accessToken);
            $responsez = $this->reserveAccount($data);
            $accountNum1 = $responsez['responseBody']['accounts'][0]['accountNumber'];
            $accountNum2 = $responsez['responseBody']['accounts'][1]['accountNumber'];
            $bankName1 = $responsez['responseBody']['accounts'][0]['bankName'];
            $bankName2 = $responsez['responseBody']['accounts'][1]['bankName'];
            // return $accountNum = $responsez['responseBody']['accounts'][1]['accountNumber'];
            $data2 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANK2
                ',
                'variable_value' => $accountNum2
            );

            $send2 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data2, $accessToken);

            $data4 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANKNAME1',
                'variable_value' => $bankName1
            );

            $send4 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data4, $accessToken);


            $data3 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANK1',
                'variable_value' => $accountNum1
            );

            $send3 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data3, $accessToken);

            $data5 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANKNAME2',
                'variable_value' => $bankName2
            );

            $send5 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data5, $accessToken);
            // return json_encode($data);
            return response()->json(['success' => 'Thank You']);

        } elseif ($req['title'] == 'register') {


            //return $req;


            $name = $req['contact']['name'];
            $ref = $req['contact']['id'];
            $phone = '0' . ltrim($req['contact']['phone'], '234');
            $bank = true;

            $data = array(
                'accountReference' => $ref,
                'accountName' => 'xxx',
                'currencyCode' => 'NGN',
                'contractCode' => '719405548416',
                'customerEmail' => $ref . '@tilde.ng',
                'customerBvn' => '21212121212',
                'customerName' => 'xxx',
                'getAllAvailableBanks' => false,
                'preferredBanks' => array('232', '035')

            );
            //return $data;

            $clientId = "ac00d5514972d6da98eb4083eed61b66";
            $clientSecret = "b344d83c7be072c0714e809ea829312b";
            $tokenEndpoint = "https://api.sendpulse.com/oauth/access_token";

            $datax = array(
                'grant_type' => 'client_credentials',
                'client_id' => $clientId,
                'client_secret' => $clientSecret
            );

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($datax),
                ),
            );
            $context  = stream_context_create($options);
            $response = file_get_contents($tokenEndpoint, false, $context);

            //return $response;

            if ($response === false) {
                // handle error
                exit("Failed to connect to the authorization server");
            }

            $responseData = json_decode($response, true);

            if (!isset($responseData['access_token'])) {
                // handle error
                exit("Failed to retrieve access token");
            }

            $accessToken = $responseData['access_token'];

            $customer = $this->getCustomer($ref);
            $current_balance = $customer['variables']['wal'];
            $dataz = array(
                'contact_id' => $ref,
                'variable_name' => 'whatsapp',
                'variable_value' => $phone
            );

            $send = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $dataz, $accessToken);
            $responsez = $this->reserveAccount($data);
            $accountNum1 = $responsez['responseBody']['accounts'][0]['accountNumber'];
            $accountNum2 = $responsez['responseBody']['accounts'][1]['accountNumber'];
            $bankName1 = $responsez['responseBody']['accounts'][0]['bankName'];
            $bankName2 = $responsez['responseBody']['accounts'][1]['bankName'];
            // return $accountNum = $responsez['responseBody']['accounts'][1]['accountNumber'];
            $data2 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANK2
                ',
                'variable_value' => $accountNum2
            );

            $send2 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data2, $accessToken);

            $data4 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANKNAME1',
                'variable_value' => $bankName1
            );

            $send4 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data4, $accessToken);


            $data3 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANK1',
                'variable_value' => $accountNum1
            );

            $send3 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data3, $accessToken);

            $data5 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANKNAME2',
                'variable_value' => $bankName2
            );

            $send5 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data5, $accessToken);
            //return json_encode($data);
            return response()->json(['success' => 'Thank You']);
        //dd($request['contact']);
        } elseif ($req['title'] == 'register2') {
            $name = $req['contact']['name'];
            $ref = $req['contact']['id'];
            $phone = '0' . ltrim($req['contact']['phone'], '234');
            $bank = true;

            $data = array(
                'accountReference' => $ref,
                'accountName' => $name,
                'currencyCode' => 'NGN',
                'contractCode' => '719405548416',
                'customerEmail' => $ref . '@tilde.ng',
                'customerBvn' => '21212121212',
                'customerName' => $name,
                'getAllAvailableBanks' => false,
                'preferredBanks' => array('232', '035')

            );
            //return $data;
            $clientId = "29100e0d3a850c1022b67e35dc9bc554";
            $clientSecret = "f30b6fc177f767e5a837653146bfd821";
            $tokenEndpoint = "https://api.sendpulse.com/oauth/access_token";

            $datax = array(
                'grant_type' => 'client_credentials',
                'client_id' => $clientId,
                'client_secret' => $clientSecret
            );

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($datax),
                ),
            );
            $context  = stream_context_create($options);
            $response = file_get_contents($tokenEndpoint, false, $context);
           // return $response;

            if ($response === false) {
                // handle error
                exit("Failed to connect to the authorization server");
            }

            $responseData = json_decode($response, true);
            //return $responseData;
            if (!isset($responseData['access_token'])) {
                //return 123;
                exit("Failed to retrieve access token");
            }

            $accessToken = $responseData['access_token'];

            $customer = $this->getCustomer2($ref);
            $current_balance = $customer['variables']['wal'];
            $dataz = array(
                'contact_id' => $ref,
                'variable_name' => 'whatsapp',
                'variable_value' => $phone
            );
            //return $dataz;
            $send = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $dataz, $accessToken);
            
            
            $responsez = $this->reserveAccount($data);

            //return $response['responseBody'];

            $accountNum1 = $responsez['responseBody']['accounts'][0]['accountNumber'];
            $accountNum2 = $responsez['responseBody']['accounts'][1]['accountNumber'];
            $bankName1 = $responsez['responseBody']['accounts'][0]['bankName'];
            $bankName2 = $responsez['responseBody']['accounts'][1]['bankName'];
            // return $accountNum = $responsez['responseBody']['accounts'][1]['accountNumber'];
            $data2 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANK2
                ',
                'variable_value' => $accountNum2
            );

            $send2 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data2, $accessToken);

            $data4 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANKNAME1',
                'variable_value' => $bankName1
            );

            $send4 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data4, $accessToken);


            $data3 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANK1',
                'variable_value' => $accountNum1
            );

            $send3 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data3, $accessToken);

            $data5 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANKNAME2',
                'variable_value' => $bankName2
            );

            $send5 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data5, $accessToken);
            return json_encode($data);
            return response()->json(['success' => 'Thank You']);

        }
    }
    public function sendpulseWebhook3(Request $request)
    {
        $req = $request[0];
        // dd($req['title'] );
        if ($req['title'] == 'register') {
            $name = $req['contact']['name'];
            $ref = $req['contact']['id'];
            $phone = '0' . ltrim($req['contact']['phone'], '234');
            $bank = true;

            $data = array(
                'accountReference' => $ref,
                'accountName' => $name,
                'currencyCode' => 'NGN',
                'contractCode' => '719405548416',
                'customerEmail' => $ref . '@tilde.ng',
                'customerBvn' => '21212121212',
                'customerName' => $name,
                'getAllAvailableBanks' => false,
                'preferredBanks' => array('232', '035')

            );

            $clientId = "ac00d5514972d6da98eb4083eed61b66";
            $clientSecret = "b344d83c7be072c0714e809ea829312b";
            $tokenEndpoint = "https://api.sendpulse.com/oauth/access_token";

            $datax = array(
                'grant_type' => 'client_credentials',
                'client_id' => $clientId,
                'client_secret' => $clientSecret
            );

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($datax),
                ),
            );
            $context  = stream_context_create($options);
            $response = file_get_contents($tokenEndpoint, false, $context);

            if ($response === false) {
                // handle error
                exit("Failed to connect to the authorization server");
            }

            $responseData = json_decode($response, true);

            if (!isset($responseData['access_token'])) {
                // handle error
                exit("Failed to retrieve access token");
            }

            $accessToken = $responseData['access_token'];

            $customer = $this->getCustomer($ref);
            $current_balance = $customer['variables']['wal'];
            $dataz = array(
                'contact_id' => $ref,
                'variable_name' => 'whatsapp',
                'variable_value' => $phone
            );

            $send = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $dataz, $accessToken);
            $responsez = $this->reserveAccount($data);
            $accountNum1 = $responsez['responseBody']['accounts'][0]['accountNumber'];
            $accountNum2 = $responsez['responseBody']['accounts'][1]['accountNumber'];
            $bankName1 = $responsez['responseBody']['accounts'][0]['bankName'];
            $bankName2 = $responsez['responseBody']['accounts'][1]['bankName'];
             //return $accountNum = $responsez['responseBody']['accounts'][1]['accountNumber'];
            $data2 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANK2
                ',
                'variable_value' => $accountNum2
            );

            $send2 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data2, $accessToken);

            $data4 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANKNAME1',
                'variable_value' => $bankName1
            );

            $send4 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data4, $accessToken);


            $data3 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANK1',
                'variable_value' => $accountNum1
            );

            $send3 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data3, $accessToken);

            $data5 = array(
                'contact_id' => $ref,
                'variable_name' => 'BANKNAME2',
                'variable_value' => $bankName2
            );

            $send5 = Send::curlPost('https://api.sendpulse.com/whatsapp/contacts/setVariable', $data5, $accessToken);
            // return json_encode($data);
            return response()->json(['success' => 'Thank You']);
        }
        // dd($request['contact']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
