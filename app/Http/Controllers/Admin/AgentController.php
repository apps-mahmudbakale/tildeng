<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agents = User::whereStatus(2)->get();
        
        return view('admin.agents.index', compact('agents'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit(User $agent)
    {
        return view('admin.agents.edit', compact('agent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $agent)
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'status' => 'required',
        ]);


        //$agent->update($request->all());

        //return redirect()->route('agents.index')->with('message', 'Agent updated successfully!');
    //}
        $agent->update($request->except('balance'));

        DB::table('wallets')
            ->where('holder_id', $agent->id)
            ->update(['balance' => $request->balance]);

        return redirect()->route('agents.index')->with('message', 'User updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $agent)
    {
        $agent->delete();
        return redirect()->back()->with('message', 'Agent deleted successfully!');
    }

}