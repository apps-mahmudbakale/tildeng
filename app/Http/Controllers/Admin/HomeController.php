<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\User;
use Bavix\Wallet\Models\Wallet;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $totalTopups = Wallet::sum('balance');
        $totalProducts = Product::whereStatus(1)->count();
        $totalCustomers = User::whereStatus(1)->count();
        $totalAgents = User::whereStatus(2)->count();
        $recentPurchases = Purchase::where('status', 1)->orderBy('id', 'desc')->take(5)->with('user')->get();
        // dd($recentPurchases);
        return view('admin.dashboard', compact('totalTopups', 'totalProducts', 'totalCustomers', 'totalAgents', 'recentPurchases'));
    }

    public function profile()
    {
        return view('admin.profile');
    }

    public function resetPassword()
    {
        return view('admin.reset-password');
    }

    public function users()
    {
        return view('admin.profile');
    }

    public function agents()
    {
        return view('admin.reset-password');
    }
}