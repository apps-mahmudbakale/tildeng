<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use App\Models\Purchase;
use App\Mail\BuyOrderEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Services\Service;

class BuyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categories = Category::all();
        $products = Product::all();
        return view('buy', compact('products', 'categories'));
    }

    public function buy(Request $request)
    {
        $purchase = new Purchase;
        $purchase->user_id = auth()->user()->id;
        $purchase->product_id = $request->product_id;
        $purchase->phone = $request->phone;
        $purchase->status = 0;

        $purchase->save();

        $plan = Product::find($request->product_id);
        $plan = $plan->network_id;

        session([
            'network' => $request->network,
            'product' => $request->product,
            'validity' => $request->validity,
            'product_id' => $request->product_id,
            'price' => $request->price,
            'phone' => $request->phone,
            'reference_no' => $purchase->reference_no,
            'plan' => $plan,
        ]);

        return redirect('confirm');
    }

    public function getConfirm()
    {
        if (session()->get('network')) {
            $network = session()->get('network');
            $product = session()->get('product');
            $validity = session()->get('validity');
            $price = session()->get('price');
            $phone = session()->get('phone');
        } else {
            return redirect('dashboard');
        }

        return view('confirm-buy', compact('network', 'product', 'price', 'validity', 'phone'));
    }

    public function postConfirm(Request $request)
    {
        if (!session()->get('product')) {
            return redirect('dashboard');
        }
        $user = auth()->user();
        //$item = Product::find(session()->get('product_id'));
        $status = 'success';
        $product = session()->get('product');
        $price = session()->get('price');
        $phone = session()->get('phone');
        $reference_no = session()->get('reference_no');

        try {
            $user->withdraw($price);
            //$user->pay($item);
            $purchase = Purchase::where('reference_no', $reference_no)->update(['status' => 1]);
            $mails = ['bakale.mahmud@gmail.com', 'abbateelde@gmail.com', 'ceo@tilde.ng'];
            $mailData = [
                "product" => $product,
                "price" => $price,
                'network' => session()->get('network'),
                'phone' => session()->get('phone')
            ];
            
            // foreach($mails as $mail){
            //     Mail::to($mail)->send(new BuyOrderEmail($mailData));
            // }

            $input = [
                'network' => 1,
                'mobile_number' => session()->get('phone'),
                'plan' => session()->get('plan'),
                'Ported_number' => true,
            ];

            $response = Service::curlPost("https://www.husmodata.com/api/data/", $input);

            // if($response['Status'] == 'successful'){
                
            // }
           
            
        } catch (\Bavix\Wallet\Exceptions\InsufficientFunds $e) {

            $status = 'error';
            $message = $e->getMessage();

            return view('finish-buy', compact('status', 'message'));
        }
        
        session()->forget(['network', 'product', 'validity', 'product_id', 'price', 'phone', 'reference_no',]);

        return view('finish-buy', compact('status', 'product', 'price', 'reference_no', 'phone'));
    }

    public function fetchProducts($id)
    {
        if (auth()->user()->status == 1) {
            $products = Product::where('category_id', $id)->where('type', 'user')->get();
        } else {
            $products = Product::where('category_id', $id)->where('type', 'agent')->get();
        }
        
        return response($products);
    }

    public function fetchPrice($id)
    {  
        $product = Product::with('category')->find($id);
        //$price = $product->price;

        return response($product);
    }
}
