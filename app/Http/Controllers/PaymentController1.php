<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Paystack;

class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        try{
            return Paystack::getAuthorizationUrl()->redirectNow();
        }catch(\Exception $e) {
            return Redirect::back()->withMessage(['msg'=>'The paystack token has expired. Please refresh the page and try again.', 'type'=>'error']);
        }        
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();
        $amount = $paymentDetails['data']['amount']/100;
        $status = $paymentDetails['data']['status'];
        $user = auth()->user();
        if ($paymentDetails['status'] === true && $status == "success") {
            $user->deposit($amount);
        }
        
        //dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
        return redirect('finish')->with('status', $status)->with('amount', $amount);
        //return view('finish', compact('status', 'amount'));
    }

    public function finish()
    {
        if (session('status')) {
            $status = session('status');
            $amount = session('amount');
        } else {
            return redirect('dashboard');
        }
        
        return view('finish', compact('status', 'amount'));
    }
}
