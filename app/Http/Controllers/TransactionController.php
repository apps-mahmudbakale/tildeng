<?php

namespace App\Http\Controllers;

use App\Models\Purchase;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index()
    {
        $transactions = Purchase::where('user_id', auth()->user()->id)->orderBy('id', 'desc')->paginate(10);
        return view('transactions', compact('transactions'));
    }

    public function topup()
    {
        $transactions = Transaction::where('payable_id', auth()->user()->id)->where('type', 'deposit')->orderBy('id', 'desc')->paginate(10);
        return view('topups', compact('transactions'));
    }
}
