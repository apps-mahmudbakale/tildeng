<?php
namespace App\Http\Controllers;

use App\Models\Purchase;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['readFromRegistry', 'writeToRegistry']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
         dd($request);
        $recentTopup = Transaction::where('payable_id', auth()->user()->id)->where('type', 'deposit')->orderBy('id', 'asc')->take(5)->get();
        $recentPurchases = Purchase::where('user_id', auth()->user()->id)->orderBy('id', 'asc')->take(5)->get();
        
        return view('dashboard', compact('recentTopup', 'recentPurchases'));
    }

    public function androidDashboard(Request $request){

        dd($request);

    }

    public function show($token)
    {
        return $token;

        return view('dashboard', ['token' => $token]);
    }

    public function profile()
    {
    	return view('profile');
    }

    public function resetPassword()
    {
    	return view('reset-password');
    }

    public function index()
    {
    	return view('hospital.index');
    }

}
