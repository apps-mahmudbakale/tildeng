<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function profilePicture(Request $request)
    {
        $user = auth()->user();
        $user->update(['image' => $request->input('fileData')]);

        return;
    }
}
