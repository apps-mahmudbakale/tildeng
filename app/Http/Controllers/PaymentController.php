<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Unicodeveloper\Paystack\Facades\Paystack;

class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        try{
            return Paystack::getAuthorizationUrl()->redirectNow();
        }catch(\Exception $e) {
            return Redirect::back()->withMessage(['msg'=>'The paystack token has expired. Please refresh the page and try again.', 'type'=>'error']);
        }        
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();
        $amount = $paymentDetails['data']['amount']/100;
        $status = $paymentDetails['data']['status'];
        $user = auth()->user();
        if ($paymentDetails['status'] === true && $status == "success") {
            $user->deposit($amount);
        }
        
        //dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
        return redirect('finish')->with('status', $status)->with('amount', $amount);
        //return view('finish', compact('status', 'amount'));
    }


    public function FundWallet($amount){

        $exist = DB::table('wallets')->where('holder_id', auth()->user()->id)->first();
        

        if($exist){
            $balance = $exist->balance +=$amount; 
            DB::table('wallets')
            ->where('holder_id', auth()->user()->id)
            ->update(
                [
                    'balance' => $balance, 
                    'holder_type' => 'App\Models\User',
                    'name' => 'Default Wallet',
                    'slug' => 'default',
                ]
            );
        }else{
            DB::table('wallets')
            ->Insert(
                [
                    'holder_id' => auth()->user()->id,
                    'balance' => $amount, 
                    'holder_type' => 'App\Models\User',
                    'name' => 'Default Wallet',
                    'slug' => 'default',
                ]
            );
        }

    

        // dd($request->all());
  
        // if($wallet = \DB::table('wallets')->where('holder_id', $request->user_id)->first()){
        //     $balance = $wallet->balance + $request->amount;
        // }else{
        //     $balance = $request->amount;
        // }
        // $fund = Wallet::updateOrCreate(
        //     ['user_id' => $request->user_id],
        //     ['cash_balance' => $balance, 'user_id' => $request->user_id]
        // );
        // \DB::table('wallets')
        // ->where('holder_id', $request->user_id)
        // ->limit(1)
        // ->update([]

        return redirect('finish')->with('status', 'success')->with('amount', $amount);
    }

    public function finish()
    {
        if (session('status')) {
            $status = session('status');
            $amount = session('amount');
        } else {
            return redirect('dashboard');
        }
        
        return view('finish', compact('status', 'amount'));
    }
}
