<?php

namespace App\Imports;

use App\Models\Card;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;


class CardImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Card([
            'serial_number' => $row['serial_number'],
            'user_id'    => auth()->user()->id
        ]);
    }

    public function rules(): array
    {
        return [
            'serial_number' => 'unique:cards'
        ];

    }

    public function customValidationMessages()
    {
        return [
            'serial_number.unique' => 'Card already exists!',
        ];
    }
}