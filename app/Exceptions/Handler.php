<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Auth;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if (in_array('admin', $exception->guards())) {
            return $request->expectsJson()
                ? response()->json([
                      'message' => $exception->getMessage()
                ], 401)
                : redirect()->guest(route('admin.login'));
        }

        if (in_array('hmo', $exception->guards())) {
            return $request->expectsJson()
                ? response()->json([
                      'message' => $exception->getMessage()
                ], 401)
                : redirect()->guest(route('hmo.login'));
        }
    
        return $request->expectsJson()
            ? response()->json([
                  'message' => $exception->getMessage()
            ], 401)
            : redirect()->guest(route('login'));
    }
}
