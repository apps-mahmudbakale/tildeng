<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebHook extends Model
{
    use HasFactory;

    protected $fillable = [
        'reference',
        'account_name',
        'account_number',
        'amount_paid',
        'name',
        'email'
    ];
}
