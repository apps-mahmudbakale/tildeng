<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    // In your Inbox model (App\Models\Inbox)
protected $guarded = ['id'];

    // In your Inbox model (App\Models\Inbox)
protected $fillable = [
    'ref', 'user_id', 'sender', 'message', 'network', 'keyword', 'created',
];

}
