<?php
namespace App\Services;

class Send {


    public static function curlPost($endpoint, $data, $token)
    {

        $data_json = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Authorization:Bearer $token",));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $response  = curl_exec($ch);
        curl_close($ch);

         return $status = json_decode($response,true);
    }


    public static function curlGet($endpoint, $token)
    {


        $ch = curl_init();
        $headers = array(
            "Accept: application/json",
            "Authorization: Bearer $token",
         );
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $response  = curl_exec($ch);
        curl_close($ch);

         return $status = json_decode($response,true);
    }

    public static function curlGet2($endpoint, $token)
    {


        $ch = curl_init();
        $headers = array(
            "Accept: application/json",
            "Authorization: Basic $token",
         );
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $response  = curl_exec($ch);
        curl_close($ch);

         return $status = json_decode($response,true);
    }

    
}